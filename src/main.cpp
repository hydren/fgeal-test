/*
 * main.cpp
 *
 *  Created on: 13 de fev de 2017
 *      Author: carlosfaruolo
 */

#include <iostream>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cmath>

#include <fgeal/fgeal.hpp>
#include <fgeal/extra/game.hpp>

using std::string;

//font shared between all states
fgeal::Font* fontMono;
float fontMonoHeight;

void globalInputHandler(fgeal::Keyboard::Key key, fgeal::Game& game, fgeal::Game::State& state)
{
	if(key >= fgeal::Keyboard::KEY_F1 and key <= fgeal::Keyboard::KEY_F10)
	{
		int nextStateId = 0;
		switch(key)
		{
			case fgeal::Keyboard::KEY_F1:
			{
				if(state.getId() < 3)
					nextStateId = state.getId() + 1;
				else
					nextStateId = 1;
				break;
			}
			case fgeal::Keyboard::KEY_F2:
			{
				if(state.getId() == 4)
					nextStateId = 5;
				else
					nextStateId = 4;
				break;
			}
			case fgeal::Keyboard::KEY_F3:
			{
				if(state.getId() == 6)
					nextStateId = 16;
				else
					nextStateId = 6;
				break;
			}
			case fgeal::Keyboard::KEY_F4:
			{
				if(state.getId() >= 7 and state.getId() < 9)
					nextStateId = state.getId() + 1;
				else
					nextStateId = 7;
				break;
			}
			case fgeal::Keyboard::KEY_F5:
			case fgeal::Keyboard::KEY_F6:
			case fgeal::Keyboard::KEY_F7:
			{
				nextStateId = 6 + (key - fgeal::Keyboard::KEY_F1);
				break;
			}
			case fgeal::Keyboard::KEY_F8:
			{
				if(state.getId() > 12)
					nextStateId = state.getId() + 1;
				if(nextStateId > 14 or state.getId() <= 12)
					nextStateId = 13;
				break;
			}
			case fgeal::Keyboard::KEY_F9:
			{
				nextStateId = 15;
				break;
			}
			default: break;
		}
		if(nextStateId > 0) try
		{
			game.enterState(nextStateId);
		}
		catch (const fgeal::GameException& exception)
		{
			std::cout << "something went wrong when trying to enter state " << nextStateId << std::endl;
		}
	}
	if(key == fgeal::Keyboard::KEY_INSERT)
		game.showFPS = !game.showFPS;
}

template <typename T> std::string toString(const T& t) { std::ostringstream os; os.precision(2); os<<t; return os.str(); }

#include "arrow_test.hxx"
#include "sprite_test.hxx"
#include "blit_test.hxx"
#include "primitives_test.hxx"
#include "color_test.hxx"
#include "font_test.hxx"
#include "keyboard_test.hxx"
#include "mouse_test.hxx"
#include "joystick_test.hxx"
#include "jukebox_test.hxx"
#include "window_test.hxx"
#include "file_test.hxx"
#include "copter_test.hxx"
#include "aircraft_test.hxx"
#include "gui_test.hxx"
#include "digits_test.hxx"

class TestGame : public fgeal::Game
{
	public:
	TestGame()
	: Game()
	{
		this->maxFps = 60;
	}

	void initialize()
	{
		srand(time(null));
		this->addState(new ArrowTestState(*this));
		this->addState(new SpriteTestState(*this));
		this->addState(new BlitTestState(*this));
		this->addState(new PrimitivesTest(*this));
		this->addState(new ColorTestState(*this));
		this->addState(new FontTestState(*this));
		this->addState(new KeyboardTest(*this));
		this->addState(new MouseTest(*this));
		this->addState(new JoystickTestState(*this));
		this->addState(new JukeboxTest(*this));
		this->addState(new WindowTest(*this));
		this->addState(new FileTestState(*this));
		this->addState(new CopterTestState(*this));
		this->addState(new AircraftTestState(*this));
		this->addState(new GUITestState(*this));
		this->addState(new DigitsTest(*this));
		this->setInputManagerEnabled();
		Game::initialize();
	}
};

int main()
{
	string title = "fgeal-test";
	title += " @ fgeal "; title += fgeal::VERSION;
	title += " using "; title += fgeal::ADAPTER_NAME;
	title += " ("; title += fgeal::ADAPTED_LIBRARY_NAME;
	title += " "; title += fgeal::ADAPTED_LIBRARY_VERSION; title += ")";

	fgeal::initialize();
	fgeal::core::absentImplementationPolicy = fgeal::core::IGNORE_BUT_PRINT_STDERR;

	fgeal::Display::Options opt;
	opt.width = 1024;
	opt.height = 768;
	opt.fullscreen = false;
	opt.title = title;
	opt.isUserResizeable = true;
	opt.iconFilename = "icon.png";
	opt.positioning = fgeal::Display::Options::POSITION_CENTERED;
//	opt.positioning = fgeal::Display::Options::POSITION_UNDEFINED;
//	opt.positioning = fgeal::Display::Options::POSITION_DEFINED;
//	opt.position.x = 64; opt.position.y = 64;
	fgeal::Display::create(opt);
//	fgeal::Display::getInstance().setIcon("icon.png");
	fontMono = new fgeal::Font("freemono.ttf");
	fontMonoHeight = fontMono->getTextHeight();

	TestGame* testGame = new TestGame();
	testGame->start();
	delete testGame;
	delete fontMono;

	fgeal::finalize();
	return EXIT_SUCCESS;
}
