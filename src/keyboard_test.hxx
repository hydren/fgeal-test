/*
 * keyboard_test.hxx
 *
 *  Created on: 15 de mai de 2018
 *      Author: carlosfaruolo
 */

#include <ciso646>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <algorithm>

#include <fgeal/fgeal.hpp>
#include <fgeal/extra/game.hpp>

class KeyboardTest : public fgeal::Game::State
{
	public:
	int getId() { return 7; }

	KeyboardTest(fgeal::Game& game)
	: State(game)
	{}

	~KeyboardTest()
	{}

	void initialize()
	{}

	void onEnter()
	{}

	void onLeave()
	{}

	void render()
	{
		using fgeal::Point;
		using fgeal::Mouse;
		using fgeal::Image;
		using fgeal::Color;

		fgeal::Display& display = game.getDisplay();
		display.clear();

		const float dw = display.getWidth(), dh = display.getHeight();
		fontMono->drawText("Keyboard arrow keys", 0.01*dw, 0.76*dh, fgeal::Color::WHITE);
		const fgeal::Point arrowUpPt1 = {0.04f*dw, 0.70f*dh}, arrowUpPt2 = {0.03f*dw, 0.72f*dh}, arrowUpPt3 = {0.05f*dw, 0.72f*dh};
		fgeal::Graphics::drawFilledTriangle(arrowUpPt1, arrowUpPt2, arrowUpPt3, fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_UP)? fgeal::Color::RED : fgeal::Color::WHITE);
		const fgeal::Point arrowDownPt1 = {0.04f*dw, 0.75f*dh}, arrowDownPt2 = {0.03f*dw, 0.73f*dh}, arrowDownPt3 = {0.05f*dw, 0.73f*dh};
		fgeal::Graphics::drawFilledTriangle(arrowDownPt1, arrowDownPt2, arrowDownPt3, fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_DOWN)? fgeal::Color::RED : fgeal::Color::WHITE);
		const fgeal::Point arrowLeftPt1 = {0, 0.74f*dh}, arrowLeftPt2 = {0.02f*dw, 0.73f*dh}, arrowLeftPt3 = {0.02f*dw, 0.75f*dh};
		fgeal::Graphics::drawFilledTriangle(arrowLeftPt1, arrowLeftPt2, arrowLeftPt3, fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_LEFT)? fgeal::Color::RED : fgeal::Color::WHITE);
		const fgeal::Point arrowRightPt1 = {0.08f*dw, 0.74f*dh}, arrowRightPt2 = {0.06f*dw, 0.73f*dh}, arrowRightPt3 = {0.06f*dw, 0.75f*dh};
		fgeal::Graphics::drawFilledTriangle(arrowRightPt1, arrowRightPt2, arrowRightPt3, fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_RIGHT)? fgeal::Color::RED : fgeal::Color::WHITE);

		float posTxt = 0.15f*dh, posTxtX = 0.10f*dw;
		#define drawKeyStatus(keyname) fontMono->drawText("Key "#keyname, posTxtX, posTxt+=fontMonoHeight, fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_##keyname)? fgeal::Color::RED : fgeal::Color::WHITE)

		drawKeyStatus(A); drawKeyStatus(B); drawKeyStatus(C); drawKeyStatus(D); drawKeyStatus(E);
		drawKeyStatus(F); drawKeyStatus(G); drawKeyStatus(H); drawKeyStatus(I); drawKeyStatus(J);
		drawKeyStatus(K); drawKeyStatus(L); drawKeyStatus(M); drawKeyStatus(N); drawKeyStatus(O);
		drawKeyStatus(P); drawKeyStatus(Q); drawKeyStatus(R); drawKeyStatus(S); drawKeyStatus(T);
		drawKeyStatus(U); drawKeyStatus(V); drawKeyStatus(W); drawKeyStatus(X); drawKeyStatus(Y);
		drawKeyStatus(Z);

		posTxt = 0.15f*dh; posTxtX = 0.20f*dw;
		drawKeyStatus(0); drawKeyStatus(1); drawKeyStatus(2); drawKeyStatus(3); drawKeyStatus(4);
		drawKeyStatus(5); drawKeyStatus(6); drawKeyStatus(7); drawKeyStatus(8); drawKeyStatus(9);

		posTxt += fontMonoHeight;
		drawKeyStatus(F1); drawKeyStatus(F2); drawKeyStatus(F3); drawKeyStatus(F4); drawKeyStatus(F5);
		drawKeyStatus(F6); drawKeyStatus(F7); drawKeyStatus(F8); drawKeyStatus(F9); drawKeyStatus(F10);
		drawKeyStatus(F11); drawKeyStatus(F12);

		posTxt += fontMonoHeight;
		drawKeyStatus(ENTER); drawKeyStatus(SPACE); drawKeyStatus(ESCAPE);
		drawKeyStatus(LEFT_CONTROL); drawKeyStatus(RIGHT_CONTROL);
		drawKeyStatus(LEFT_SHIFT); drawKeyStatus(RIGHT_SHIFT);
		drawKeyStatus(LEFT_ALT); drawKeyStatus(RIGHT_ALT);
		drawKeyStatus(LEFT_SUPER); drawKeyStatus(RIGHT_SUPER);
		drawKeyStatus(MENU); drawKeyStatus(TAB); drawKeyStatus(BACKSPACE);

		posTxt = 0.15f*dh; posTxtX = 0.35f*dw;

		posTxt += fontMonoHeight;
		drawKeyStatus(MINUS); drawKeyStatus(EQUALS);
		drawKeyStatus(LEFT_BRACKET); drawKeyStatus(RIGHT_BRACKET);
		drawKeyStatus(SEMICOLON); drawKeyStatus(COMMA); drawKeyStatus(PERIOD);
		drawKeyStatus(SLASH); drawKeyStatus(BACKSLASH); drawKeyStatus(QUOTE); drawKeyStatus(TILDE);

		posTxt += fontMonoHeight;
		drawKeyStatus(INSERT); drawKeyStatus(DELETE); drawKeyStatus(HOME); drawKeyStatus(END);
		drawKeyStatus(PAGE_UP); drawKeyStatus(PAGE_DOWN);

		posTxt += fontMonoHeight;
		drawKeyStatus(NUMPAD_1); drawKeyStatus(NUMPAD_2); drawKeyStatus(NUMPAD_3); drawKeyStatus(NUMPAD_4);
		drawKeyStatus(NUMPAD_5); drawKeyStatus(NUMPAD_6); drawKeyStatus(NUMPAD_7); drawKeyStatus(NUMPAD_8);
		drawKeyStatus(NUMPAD_9); drawKeyStatus(NUMPAD_0);

		posTxt += fontMonoHeight;
		drawKeyStatus(NUMPAD_ADDITION);
		drawKeyStatus(NUMPAD_SUBTRACTION);
		drawKeyStatus(NUMPAD_MULTIPLICATION);
		drawKeyStatus(NUMPAD_DIVISION);
		drawKeyStatus(NUMPAD_DECIMAL);
		drawKeyStatus(NUMPAD_ENTER);

		posTxt = 16;
		fontMono->drawText("Keyboard test (press F4 again for mouse test)", 16, posTxt+=fontMonoHeight, fgeal::Color::GREEN);
		fontMono->drawText("A list of mapped keys will be shown up, and currently pressed ones will appear red instead of white", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Pressing arrow keys also trigger std::cout event messages", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);

		fontMono->drawText("Press F1 through F9 keys for other tests", 16, game.getDisplay().getHeight() - fontMonoHeight, fgeal::Color::WHITE);
	}

	void update(float delta)
	{}

	void onKeyPressed(fgeal::Keyboard::Key key)
	{
		globalInputHandler(key, game, *this);

		switch(key)
		{
			case fgeal::Keyboard::KEY_ARROW_UP:    std::cout << "pressed arrow up" << std::endl; break;
			case fgeal::Keyboard::KEY_ARROW_DOWN:  std::cout << "pressed arrow down" << std::endl; break;
			case fgeal::Keyboard::KEY_ARROW_LEFT:  std::cout << "pressed arrow left" << std::endl; break;
			case fgeal::Keyboard::KEY_ARROW_RIGHT: std::cout << "pressed arrow right" << std::endl; break;
			default: break;
		}

	}

	void onKeyReleased(fgeal::Keyboard::Key key)
	{
		switch(key)
		{
			case fgeal::Keyboard::KEY_ARROW_UP:    std::cout << "released arrow up" << std::endl; break;
			case fgeal::Keyboard::KEY_ARROW_DOWN:  std::cout << "released arrow down" << std::endl; break;
			case fgeal::Keyboard::KEY_ARROW_LEFT:  std::cout << "released arrow left" << std::endl; break;
			case fgeal::Keyboard::KEY_ARROW_RIGHT: std::cout << "released arrow right" << std::endl; break;
			default: break;
		}
	}
};
