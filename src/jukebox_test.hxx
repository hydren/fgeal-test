/*
 * jukebox_test.hxx
 *
 *  Created on: 22 de fev de 2017
 *      Author: carlosfaruolo
 */

#include <ciso646>
#include <iostream>
#include <cstdlib>
#include <cmath>

#include <fgeal/fgeal.hpp>
#include <fgeal/extra/game.hpp>

bool isWithinRectangle(const fgeal::Rectangle& rect, float x, float y)
{
	return (x > rect.x and x < rect.x + rect.w
		and y > rect.y and y < rect.y + rect.h);
}

class JukeboxTest : public fgeal::Game::State
{
	const float buttonWidth, buttonHeight;
	fgeal::Rectangle rectJukebox;

	fgeal::Rectangle rectMusicPlay, rectMusicPause, rectMusicResume, rectMusicStop, rectMusicChange, rectMusicVolUp, rectMusicVolDown;
	fgeal::Rectangle rectSoundPlay, rectSoundPause, rectSoundResume, rectSoundStop, rectSoundChange, rectSoundVolUp, rectSoundVolDown, rectSoundPitUp, rectSoundPitDown;

	fgeal::Music* music[2];
	fgeal::Sound* sound[5];
	fgeal::Color colors[5];
	unsigned musicNumber, soundNumber;

	std::string status[3];

	public:
	int getId() { return 10; }

	JukeboxTest(fgeal::Game& game)
	: State(game),
	  buttonWidth(96), buttonHeight(32),
	  musicNumber(1), soundNumber(1)
	{}

	~JukeboxTest()
	{
		for(unsigned i = 0; i < 2; i++)
			delete music[i];

		for(unsigned i = 0; i < 5; i++)
			delete sound[i];
	}

	void initialize()
	{
		fgeal::Display& display = game.getDisplay();

		fgeal::Rectangle jukeSize = {(display.getWidth() - 8*buttonWidth)/2, (display.getHeight() - 1.5f*buttonHeight)/2, 8*buttonWidth, 4*buttonHeight};
		rectJukebox = jukeSize;

		fgeal::Rectangle btnSize = { (display.getWidth() - 6*buttonWidth)/2, (display.getHeight() - buttonHeight)/2, buttonWidth, buttonHeight };
		rectMusicPlay = btnSize;
		rectSoundPlay = rectMusicPlay;
		rectSoundPlay.y += buttonHeight*1.5;

		rectMusicStop = rectMusicPlay;
		rectMusicStop.x += buttonWidth*1.125;
		rectMusicStop.w = 48;

		rectMusicPause = rectMusicPlay;
		rectMusicPause.x += buttonWidth*1.750;
		rectMusicPause.w = 48;

		rectMusicResume = rectMusicPlay;
		rectMusicResume.x += buttonWidth*2.375;
		rectMusicResume.w = 48;

		rectMusicChange = rectMusicPlay;
		rectMusicChange.x += buttonWidth*3.000;
		rectMusicChange.w = 48;

		rectMusicVolUp = rectMusicChange;
		rectMusicVolUp.x += rectMusicChange.w + 8;
		rectMusicVolUp.h = 0.4*rectMusicChange.h - 4;
		rectMusicVolUp.w *= 0.5;

		rectMusicVolDown = rectMusicVolUp;
		rectMusicVolDown.y = rectMusicChange.y + rectMusicChange.h - rectMusicVolDown.h;

		rectSoundStop = rectSoundPlay;
		rectSoundStop.x += buttonWidth*1.125;
		rectSoundStop.w = 48;

		rectSoundPause = rectSoundPlay;
		rectSoundPause.x += buttonWidth*1.750;
		rectSoundPause.w = 48;

		rectSoundResume = rectSoundPlay;
		rectSoundResume.x += buttonWidth*2.375;
		rectSoundResume.w = 48;

		rectSoundChange = rectSoundPlay;
		rectSoundChange.x += buttonWidth*3.000;
		rectSoundChange.w = 48;

		rectSoundVolUp = rectSoundChange;
		rectSoundVolUp.x += rectSoundChange.w + 8;
		rectSoundVolUp.h = 0.4*rectSoundChange.h - 4;
		rectSoundVolUp.w *= 0.5;

		rectSoundVolDown = rectSoundVolUp;
		rectSoundVolDown.y = rectSoundChange.y + rectSoundChange.h - rectSoundVolDown.h;

		rectSoundPitUp = rectSoundVolUp;
		rectSoundPitUp.x += rectSoundVolUp.w*3.5;

		rectSoundPitDown = rectSoundVolDown;
		rectSoundPitDown.x += rectSoundVolDown.w*3.5;

		for(unsigned i = 0; i < 2; i++)
			music[i] = new fgeal::Music(std::string("music")+toString(i+1)+".ogg");

		for(unsigned i = 0; i < 5; i++)
		{
			sound[i] = new fgeal::Sound(std::string("sound")+toString(i+1)+".ogg");
			sound[i]->setPlaybackSpeed(1.0f, true);
		}

		colors[0] = fgeal::Color::WHITE;
		colors[1] = fgeal::Color::BLUE;
		colors[2] = fgeal::Color::RED;
		colors[3] = fgeal::Color::GREEN;
		colors[4] = fgeal::Color::YELLOW;

		// maintain equal sized texts
		status[0] = "stopped    ";
		status[1] = "paused     ";
		status[2] = "playing... ";
	}

	void onEnter()
	{
		soundNumber = musicNumber = 0;
	}

	void onLeave()
	{
		for(unsigned i = 0; i < 2; i++)
			music[i]->stop();

		for(unsigned i = 0; i < 5; i++)
			sound[i]->stop();
	}

	void render()
	{
		char tmp[128];

		fgeal::Display& display = game.getDisplay();
		fgeal::Graphics::drawFilledRectangle(0, 0, display.getWidth(), display.getHeight(), fgeal::Color::NAVY);

		fgeal::Graphics::drawFilledRectangle(rectJukebox, fgeal::Color::DARK_GREY);

		fgeal::Graphics::drawFilledRectangle(rectMusicPlay, fgeal::Color::ROSE);
		fontMono->drawText("Play music", rectMusicPlay.x + 8, rectMusicPlay.y + 4, fgeal::Color::BLACK);
		fgeal::Graphics::drawFilledRectangle(rectMusicPlay.x+2, rectMusicPlay.y+2, 4, 4, fgeal::Color::BLACK);
		fgeal::Graphics::drawFilledRectangle(rectMusicPlay.x+3, rectMusicPlay.y+3, 2, 2, colors[musicNumber]);

		fgeal::Graphics::drawFilledRectangle(rectMusicStop, fgeal::Color::PURPLE);
		fontMono->drawText("Stop", rectMusicStop.x + 4, rectMusicStop.y + 4, fgeal::Color::WHITE);

		fgeal::Graphics::drawFilledRectangle(rectMusicPause, fgeal::Color::PURPLE);
		fontMono->drawText("Pause", rectMusicPause.x + 4, rectMusicPause.y + 4, fgeal::Color::WHITE);

		fgeal::Graphics::drawFilledRectangle(rectMusicResume, fgeal::Color::PURPLE);
		fontMono->drawText("Resume", rectMusicResume.x + 4, rectMusicResume.y + 4, fgeal::Color::WHITE);

		fgeal::Graphics::drawFilledRectangle(rectMusicChange, fgeal::Color::PURPLE);
		fontMono->drawText("Change", rectMusicChange.x + 4, rectMusicChange.y + 4, fgeal::Color::WHITE);

		fgeal::Graphics::drawFilledTriangle(rectMusicVolUp.x + rectMusicVolUp.w/2, rectMusicVolUp.y, rectMusicVolUp.x, rectMusicVolUp.y + rectMusicVolUp.h, rectMusicVolUp.x + rectMusicVolUp.w, rectMusicVolUp.y + rectMusicVolUp.h, fgeal::Color::RED);
		sprintf(tmp, "%2.2f", music[musicNumber]->getVolume());
		fontMono->drawText(std::string("Vol:")+tmp, rectMusicVolUp.x, rectMusicVolUp.y + rectMusicVolUp.h, fgeal::Color::WHITE);
		fgeal::Graphics::drawFilledTriangle(rectMusicVolDown.x + rectMusicVolDown.w/2, rectMusicVolDown.y + rectMusicVolDown.h, rectMusicVolDown.x, rectMusicVolDown.y, rectMusicVolDown.x + rectMusicVolDown.w, rectMusicVolDown.y, fgeal::Color::MAROON);

		const unsigned musicStatus = music[musicNumber]->isPlaying()? 2 : music[musicNumber]->isPaused()? 1 : 0;
		fontMono->drawText(status[musicStatus], rectMusicPlay.x-fontMono->getTextWidth(*status), rectMusicVolUp.y + rectMusicVolUp.h, fgeal::Color::WHITE);

		fgeal::Graphics::drawFilledRectangle(rectSoundPlay, fgeal::Color::CHARTREUSE);
		fontMono->drawText("Play sound", rectSoundPlay.x + 8, rectSoundPlay.y + 4, fgeal::Color::BLACK);
		fgeal::Graphics::drawFilledRectangle(rectSoundPlay.x+2, rectSoundPlay.y+2, 4, 4, fgeal::Color::BLACK);
		fgeal::Graphics::drawFilledRectangle(rectSoundPlay.x+3, rectSoundPlay.y+3, 2, 2, colors[soundNumber]);

		fgeal::Graphics::drawFilledRectangle(rectSoundStop, fgeal::Color::DARK_GREEN);
		fontMono->drawText("Stop", rectSoundStop.x + 4, rectSoundStop.y + 4, fgeal::Color::WHITE);

		fgeal::Graphics::drawFilledRectangle(rectSoundPause, fgeal::Color::DARK_GREEN);
		fontMono->drawText("Pause", rectSoundPause.x + 4, rectSoundPause.y + 4, fgeal::Color::WHITE);

		fgeal::Graphics::drawFilledRectangle(rectSoundResume, fgeal::Color::DARK_GREEN);
		fontMono->drawText("Resume", rectSoundResume.x + 4, rectSoundResume.y + 4, fgeal::Color::WHITE);

		fgeal::Graphics::drawFilledRectangle(rectSoundChange, fgeal::Color::DARK_GREEN);
		fontMono->drawText("Change", rectSoundChange.x + 4, rectSoundChange.y + 4, fgeal::Color::WHITE);

		fgeal::Graphics::drawFilledTriangle(rectSoundVolUp.x + rectSoundVolUp.w/2, rectSoundVolUp.y, rectSoundVolUp.x, rectSoundVolUp.y + rectSoundVolUp.h, rectSoundVolUp.x + rectSoundVolUp.w, rectSoundVolUp.y + rectSoundVolUp.h, fgeal::Color::CYAN);
		sprintf(tmp, "%2.2f", sound[soundNumber]->getVolume());
		fontMono->drawText(std::string("Vol:")+tmp, rectSoundVolUp.x, rectSoundVolUp.y + rectSoundVolUp.h, fgeal::Color::WHITE);
		fgeal::Graphics::drawFilledTriangle(rectSoundVolDown.x + rectSoundVolDown.w/2, rectSoundVolDown.y + rectSoundVolDown.h, rectSoundVolDown.x, rectSoundVolDown.y, rectSoundVolDown.x + rectSoundVolDown.w, rectSoundVolDown.y, fgeal::Color::TEAL);

		fgeal::Graphics::drawFilledTriangle(rectSoundPitUp.x + rectSoundPitUp.w/2, rectSoundPitUp.y, rectSoundPitUp.x, rectSoundPitUp.y + rectSoundPitUp.h, rectSoundPitUp.x + rectSoundPitUp.w, rectSoundPitUp.y + rectSoundPitUp.h, fgeal::Color::YELLOW);
		sprintf(tmp, "%2.2f", sound[soundNumber]->getPlaybackSpeed());
		fontMono->drawText(std::string("Pitch:")+tmp, rectSoundPitUp.x, rectSoundPitUp.y + rectSoundPitUp.h, fgeal::Color::WHITE);
		fgeal::Graphics::drawFilledTriangle(rectSoundPitDown.x + rectSoundPitDown.w/2, rectSoundPitDown.y + rectSoundPitDown.h, rectSoundPitDown.x, rectSoundPitDown.y, rectSoundPitDown.x + rectSoundPitDown.w, rectSoundPitDown.y, fgeal::Color::OLIVE);

		const unsigned soundStatus = sound[soundNumber]->isPlaying()? 2 : sound[soundNumber]->isPaused()? 1 : 0;
		fontMono->drawText(status[soundStatus], rectSoundPlay.x-fontMono->getTextWidth(*status), rectSoundPitUp.y + rectSoundPitUp.h, fgeal::Color::WHITE);

		float posTxt = 16;
		fontMono->drawText("Jukebox test", 16, posTxt+=fontMonoHeight, fgeal::Color::GREEN);
		fontMono->drawText("Click on the play button to play the music or sound", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Click on the pause button to pause the music or sound", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Click on the stop button to stop the music or sound", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Click on the change button to alternate between the available musics or sounds", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("The small rectangle indicates which music or sound is selected", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Click on the volume controls to change volume of music or sounds.", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Click on the pitch controls to change pitch of sounds.", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("The status of the currently selected music of sound is drawn right of the controls", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);

		fontMono->drawText("Press F1 through F9 keys for other tests", 16, game.getDisplay().getHeight() - fontMonoHeight, fgeal::Color::WHITE);
	}

	void update(float delta)
	{}

	void onKeyPressed(fgeal::Keyboard::Key key)
	{
		globalInputHandler(key, game, *this);
	}

	void onMouseButtonPressed(fgeal::Mouse::Button button, int x, int y)
	{
		if(button == fgeal::Mouse::BUTTON_LEFT)
		{
			if(isWithinRectangle(rectMusicPlay, x, y))
			{
				music[musicNumber]->play();
			}

			if(isWithinRectangle(rectMusicStop, x, y))
			{
				music[musicNumber]->stop();
			}

			if(isWithinRectangle(rectMusicPause, x, y))
			{
				music[musicNumber]->pause();
			}

			if(isWithinRectangle(rectMusicResume, x, y))
			{
				music[musicNumber]->resume();
			}

			if(isWithinRectangle(rectMusicChange, x, y))
			{
				if(musicNumber < 1) musicNumber++;
				else musicNumber = 0;
			}

			if(isWithinRectangle(rectMusicVolUp, x, y))
			{
				music[musicNumber]->setVolume(music[musicNumber]->getVolume() + 0.1);
			}
			if(isWithinRectangle(rectMusicVolDown, x, y))
			{
				music[musicNumber]->setVolume(std::max(music[musicNumber]->getVolume() - 0.1, 0.1));
			}

			if(isWithinRectangle(rectSoundPlay, x, y))
			{
				if(soundNumber < 4)
					sound[soundNumber]->play();
				else
					sound[4]->loop();
			}

			if(isWithinRectangle(rectSoundStop, x, y))
			{
				sound[soundNumber]->stop();
			}

			if(isWithinRectangle(rectSoundPause, x, y))
			{
				sound[soundNumber]->pause();
			}

			if(isWithinRectangle(rectSoundResume, x, y))
			{
				sound[soundNumber]->resume();
			}

			if(isWithinRectangle(rectSoundChange, x, y))
			{
				if(soundNumber < 4) soundNumber++;
				else soundNumber = 0;
			}

			if(isWithinRectangle(rectSoundVolUp, x, y))
			{
				sound[soundNumber]->setVolume(sound[soundNumber]->getVolume() + 0.1);
			}
			if(isWithinRectangle(rectSoundVolDown, x, y))
			{
				sound[soundNumber]->setVolume(std::max(sound[soundNumber]->getVolume() - 0.1, 0.1));
			}

			if(isWithinRectangle(rectSoundPitUp, x, y))
			{
				sound[soundNumber]->setPlaybackSpeed(sound[soundNumber]->getPlaybackSpeed() + 0.1, true);
			}
			if(isWithinRectangle(rectSoundPitDown, x, y))
			{
				sound[soundNumber]->setPlaybackSpeed(std::max(sound[soundNumber]->getPlaybackSpeed() - 0.1, 0.1), true);
			}
		}
	}
};
