/*
 * primitives_test.hxx
 *
 *  Created on: 4 de jul de 2017
 *      Author: carlosfaruolo
 */

#include <ciso646>
#include <cmath>

#include <fgeal/fgeal.hpp>
#include <fgeal/extra/game.hpp>
#include <fgeal/extra/primitives.hpp>

class PrimitivesTest : public fgeal::Game::State
{
	fgeal::Point horizontalLine[2], slantedLine[2], horizontalThickLine[2], slantedThickLine[2], triangle[3], quadrangle[4], polygon[6];
	fgeal::Point circleCenter, ellipseCenter, radii, arcCenter, thickArcCenter, pieCenter;
	fgeal::Rectangle rectangle, roundedRectangle;
	float radius, arcAngleStart, arcAngleEnd, lineThickness;

	bool whiteBg;

	enum FillType { FILL_NONE, FILL_TRANSPARENT, FILL_OPAQUE, FILL_TYPE_COUNT } fill;

	public:
	int getId() { return 4; }

	PrimitivesTest(fgeal::Game& game)
	: State(game), radius(0), arcAngleStart(0), arcAngleEnd(0), lineThickness(1.5), whiteBg(false), fill()
	{}

	~PrimitivesTest()
	{}

	void initialize()
	{}

	void onEnter()
	{
		fgeal::Display& display = game.getDisplay();
		const float w = display.getWidth(), h = display.getHeight();

		lineThickness = 0.005*h;

		horizontalLine[0].x = 0.05*w; horizontalLine[0].y = 0.25*h;
		horizontalLine[1].x = 0.40*w; horizontalLine[1].y = 0.25*h;

		slantedLine[0].x = 0.05*w; slantedLine[0].y = 0.275*h;
		slantedLine[1].x = 0.40*w; slantedLine[1].y = 0.325*h;

		horizontalThickLine[0].x = 0.45*w; horizontalThickLine[0].y = 0.25*h;
		horizontalThickLine[1].x = 0.60*w; horizontalThickLine[1].y = 0.25*h;

		slantedThickLine[0].x = 0.45*w; slantedThickLine[0].y = 0.275*h;
		slantedThickLine[1].x = 0.60*w; slantedThickLine[1].y = 0.325*h;

		rectangle.x = 0.05*w; rectangle.y = 0.375*h;
		rectangle.w = 0.25*w; rectangle.h = 0.15*h;

		roundedRectangle.x = 0.35*w; roundedRectangle.y = 0.375*h;
		roundedRectangle.w = 0.25*w; roundedRectangle.h = 0.15*h;

		triangle[0].x = 0.20*w;  triangle[0].y = 0.75*h;
		triangle[1].x = 0.30*w;  triangle[1].y = 0.75*h;
		triangle[2].x = 0.25*w;  triangle[2].y = 0.60*h;

		quadrangle[0].x = 0.30*w; quadrangle[0].y = 0.6*h;
		quadrangle[1].x = 0.40*w; quadrangle[1].y = 0.6*h;
		quadrangle[2].x = 0.45*w; quadrangle[2].y = 0.7*h;
		quadrangle[3].x = 0.40*w; quadrangle[3].y = 0.7*h;

		radius = 0.08*h;
		arcCenter.x = 0.725*w; arcCenter.y = 0.275*h;
		thickArcCenter.x = 0.9*w; thickArcCenter.y = 0.275*h;
		circleCenter.x = 0.725*w; circleCenter.y = 0.45*h;
		pieCenter.x = 0.9*w; pieCenter.y = 0.45*h;

		radii.x = 0.1*w;
		radii.y = 0.1*h;
		ellipseCenter.x = 0.8*w; ellipseCenter.y = 0.65*h;

		polygon[0].x = 0.52*w; polygon[0].y = 0.55*h;
		polygon[1].x = 0.61*w; polygon[1].y = 0.55*h;
		polygon[2].x = 0.65*w; polygon[2].y = 0.65*h;
		polygon[3].x = 0.61*w; polygon[3].y = 0.75*h;
		polygon[4].x = 0.52*w; polygon[4].y = 0.75*h;
		polygon[5].x = 0.48*w; polygon[5].y = 0.65*h;

		fill = FILL_NONE;
		arcAngleStart = M_PI_2;
		arcAngleEnd = M_PI;
	}

	void onLeave() {}

	void render()
	{
		fgeal::Display& display = game.getDisplay();
		display.clear();
		if(whiteBg) fgeal::Graphics::drawFilledRectangle(0, 0, display.getWidth(), display.getHeight());

		fgeal::Graphics::drawLine(horizontalLine[0], horizontalLine[1], fgeal::Color::GREEN);
		fgeal::Graphics::drawLine(slantedLine[0], slantedLine[1], fgeal::Color::GREEN);

		fgeal::Graphics::drawThickLine(horizontalThickLine[0], horizontalThickLine[1], lineThickness, fgeal::Color::GREEN);
		fgeal::Graphics::drawThickLine(slantedThickLine[0], slantedThickLine[1], lineThickness, fgeal::Color::GREEN);

		if(fill == FILL_NONE)
			fgeal::Graphics::drawTriangle(triangle[0], triangle[1], triangle[2], fgeal::Color::RED);
		else
			fgeal::Graphics::drawFilledTriangle(triangle[0], triangle[1], triangle[2], fgeal::Color::RED.getTransparent(fill == FILL_OPAQUE? 255 : 128));

		if(fill == FILL_NONE)
			fgeal::Graphics::drawRectangle(rectangle, fgeal::Color::BLUE);
		else
			fgeal::Graphics::drawFilledRectangle(rectangle, fgeal::Color::BLUE.getTransparent(fill == FILL_OPAQUE? 255 : 128));

		if(fill == FILL_NONE)
			fgeal::Graphics::drawRoundedRectangle(roundedRectangle, 0.2f*radius, fgeal::Color::DARK_GREEN);
		else
			fgeal::Graphics::drawFilledRoundedRectangle(roundedRectangle, 0.2f*radius, fgeal::Color::DARK_GREEN.getTransparent(fill == FILL_OPAQUE? 255 : 128));

		if(fill == FILL_NONE)
			fgeal::Graphics::drawQuadrangle(quadrangle[0], quadrangle[1], quadrangle[2], quadrangle[3], fgeal::Color::YELLOW);
		else
			fgeal::Graphics::drawFilledQuadrangle(quadrangle[0], quadrangle[1], quadrangle[2], quadrangle[3], fgeal::Color::YELLOW.getTransparent(fill == FILL_OPAQUE? 255 : 128));

		if(fill == FILL_NONE)
			fgeal::Graphics::drawCircle(circleCenter, radius, fgeal::Color::MAGENTA);
		else
			fgeal::Graphics::drawFilledCircle(circleCenter, radius, fgeal::Color::MAGENTA.getTransparent(fill == FILL_OPAQUE? 255 : 128));

		if(fill == FILL_NONE)
			fgeal::Graphics::drawEllipse(ellipseCenter, radii, fgeal::Color::CYAN);
		else
			fgeal::Graphics::drawFilledEllipse(ellipseCenter, radii, fgeal::Color::CYAN.getTransparent(fill == FILL_OPAQUE? 255 : 128));

		if(fill == FILL_NONE)
			fgeal::Graphics::drawPolygon(polygon, sizeof(polygon)/sizeof(*polygon), fgeal::Color::ORANGE);
		else
			fgeal::Graphics::drawFilledPolygon(polygon, sizeof(polygon)/sizeof(*polygon), fgeal::Color::ORANGE.getTransparent(fill == FILL_OPAQUE? 255 : 128));

		if(fill == FILL_NONE)
			fgeal::Graphics::drawCircleSector(pieCenter.x, pieCenter.y, radius, arcAngleStart, arcAngleEnd, fgeal::Color::MAROON);
		else
			fgeal::Graphics::drawFilledCircleSector(pieCenter.x, pieCenter.y, radius, arcAngleStart, arcAngleEnd, fgeal::Color::MAROON.getTransparent(fill == FILL_OPAQUE? 255 : 128));

		fgeal::Graphics::drawArc(arcCenter.x, arcCenter.y, radius, arcAngleStart, arcAngleEnd, fgeal::Color::TEAL);

		fgeal::Graphics::drawThickArc(thickArcCenter.x, thickArcCenter.y, radius, lineThickness, arcAngleStart, arcAngleEnd, fgeal::Color::TEAL);

		const fgeal::Color textColor = whiteBg? fgeal::Color::BLACK : fgeal::Color::WHITE;
		float posTxt = 16;
		fontMono->drawText("Primitives test (press F2 again for color test)", 16, posTxt+=fontMonoHeight, fgeal::Color::GREEN);
		fontMono->drawText("Press enter key to change toggle fill mode.", 16, posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press space to switch the background from white/black", 16, posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press shift to toogle faster primitives flag", 16, posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Arrow keys controls positions of all figures", 16, posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Z-X keys controls ellipse x-radius and circle radius", 16, posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("C-V keys controls ellipse y-radius", 16, posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("R-T/F-G keys controls the angles of both the arc and the circular sector", 16, posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("A-Q keys controls the thickness of thick primitives", 16, posTxt+=fontMonoHeight, textColor);

		fontMono->drawText("Press F1 through F9 keys for other tests", 16, game.getDisplay().getHeight() - fontMonoHeight, textColor);
	}

	void update(float delta)
	{}

	void onKeyPressed(fgeal::Keyboard::Key key)
	{
		globalInputHandler(key, game, *this);

		if(key == fgeal::Keyboard::KEY_ENTER)
			fill = static_cast<FillType>((fill+1) % FILL_TYPE_COUNT);

		if(key == fgeal::Keyboard::KEY_SPACE)
			whiteBg = !whiteBg;

		if(key == fgeal::Keyboard::KEY_LEFT_SHIFT or key == fgeal::Keyboard::KEY_RIGHT_SHIFT)
			fgeal::Graphics::useFasterPrimitivesHint = !fgeal::Graphics::useFasterPrimitivesHint;

		const float d = fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_LEFT_SHIFT)? 0.1 : 1.0;

		if(key == fgeal::Keyboard::KEY_ARROW_UP)
		{
			horizontalLine[0].y-=d; horizontalLine[1].y-=d;
			slantedLine[0].y-=d; slantedLine[1].y-=d;
			triangle[0].y-=d; triangle[1].y-=d; triangle[2].y-=d;
			rectangle.y-=d;
			quadrangle[0].y-=d; quadrangle[1].y-=d; quadrangle[2].y-=d; quadrangle[3].y-=d;
			circleCenter.y-=d;
			ellipseCenter.y-=d;
			polygon[0].y-=d; polygon[1].y-=d; polygon[2].y-=d; polygon[3].y-=d; polygon[4].y-=d; polygon[5].y-=d;
			roundedRectangle.y-=d;
			arcCenter.y-=d;
			pieCenter.y-=d;
			thickArcCenter.y-=d;
		}

		if(key == fgeal::Keyboard::KEY_ARROW_DOWN)
		{
			horizontalLine[0].y+=d; horizontalLine[1].y+=d;
			slantedLine[0].y+=d; slantedLine[1].y+=d;
			triangle[0].y+=d; triangle[1].y+=d; triangle[2].y+=d;
			rectangle.y+=d;
			quadrangle[0].y+=d; quadrangle[1].y+=d; quadrangle[2].y+=d; quadrangle[3].y+=d;
			circleCenter.y+=d;
			ellipseCenter.y+=d;
			polygon[0].y+=d; polygon[1].y+=d; polygon[2].y+=d; polygon[3].y+=d; polygon[4].y+=d; polygon[5].y+=d;
			roundedRectangle.y+=d;
			arcCenter.y+=d;
			pieCenter.y+=d;
			thickArcCenter.y+=d;
		}

		if(key == fgeal::Keyboard::KEY_ARROW_LEFT)
		{
			horizontalLine[0].x-=d; horizontalLine[1].x-=d;
			slantedLine[0].x-=d; slantedLine[1].x-=d;
			triangle[0].x-=d; triangle[1].x-=d; triangle[2].x-=d;
			rectangle.x-=d;
			quadrangle[0].x-=d; quadrangle[1].x-=d; quadrangle[2].x-=d; quadrangle[3].x-=d;
			circleCenter.x-=d;
			ellipseCenter.x-=d;
			polygon[0].x-=d; polygon[1].x-=d; polygon[2].x-=d; polygon[3].x-=d; polygon[4].x-=d; polygon[5].x-=d;
			roundedRectangle.x-=d;
			arcCenter.x-=d;
			pieCenter.x-=d;
			thickArcCenter.x-=d;
		}

		if(key == fgeal::Keyboard::KEY_ARROW_RIGHT)
		{
			horizontalLine[0].x+=d; horizontalLine[1].x+=d;
			slantedLine[0].x+=d; slantedLine[1].x+=d;
			triangle[0].x+=d; triangle[1].x+=d; triangle[2].x+=d;
			rectangle.x+=d;
			quadrangle[0].x+=d; quadrangle[1].x+=d; quadrangle[2].x+=d; quadrangle[3].x+=d;
			circleCenter.x+=d;
			ellipseCenter.x+=d;
			polygon[0].x+=d; polygon[1].x+=d; polygon[2].x+=d; polygon[3].x+=d; polygon[4].x+=d; polygon[5].x+=d;
			roundedRectangle.x+=d;
			arcCenter.x+=d;
			pieCenter.x+=d;
			thickArcCenter.x+=d;
		}

		if(key == fgeal::Keyboard::KEY_X)
		{
			radius++;
			radii.x++;
		}

		if(key == fgeal::Keyboard::KEY_Z)
		{
			radius--;
			radii.x--;
		}

		if(key == fgeal::Keyboard::KEY_C)
		{
			radii.y--;
		}

		if(key == fgeal::Keyboard::KEY_V)
		{
			radii.y++;
		}

		if(key == fgeal::Keyboard::KEY_A)
		{
			lineThickness -= 0.25f;
		}

		if(key == fgeal::Keyboard::KEY_Q)
		{
			lineThickness += 0.25f;
		}

		if(key == fgeal::Keyboard::KEY_R)
		{
			arcAngleEnd += M_PI * 0.05;
		}

		if(key == fgeal::Keyboard::KEY_T)
		{
			arcAngleEnd -= M_PI * 0.05;
		}

		if(key == fgeal::Keyboard::KEY_F)
		{
			arcAngleStart += M_PI * 0.05;
		}

		if(key == fgeal::Keyboard::KEY_G)
		{
			arcAngleStart -= M_PI * 0.05;
		}
	}
};
