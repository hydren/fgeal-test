/*
 * mouse_test.hxx
 *
 *  Created on: 22 de fev de 2017
 *      Author: carlosfaruolo
 */

#include <ciso646>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <algorithm>

#include <fgeal/fgeal.hpp>
#include <fgeal/extra/game.hpp>

class MouseTest : public fgeal::Game::State
{
	fgeal::Rectangle outline;
	bool outlineOn;
	fgeal::Color outlineColor;
	int knobY;

	public:
	int getId() { return 8; }

	MouseTest(fgeal::Game& game)
	: State(game), outlineOn(false), knobY(0)
	{}

	~MouseTest()
	{}

	void initialize()
	{}

	void onEnter()
	{
		outlineOn = false;
		outline = fgeal::Rectangle();
		knobY = 0;
	}

	void onLeave() {}

	void render()
	{
		using fgeal::Point;
		using fgeal::Mouse;
		using fgeal::Color;

		fgeal::Display& display = game.getDisplay();
		display.clear();

		const float crosshairSize = 0.05*std::min(display.getWidth(), display.getHeight());
		Point crosshair[4];
		crosshair[0].x = Mouse::getPositionX(); crosshair[0].y = Mouse::getPositionY() - crosshairSize;
		crosshair[1].x = Mouse::getPositionX(); crosshair[1].y = Mouse::getPositionY() + crosshairSize;
		crosshair[2].x = Mouse::getPositionX() - crosshairSize; crosshair[2].y = Mouse::getPositionY();
		crosshair[3].x = Mouse::getPositionX() + crosshairSize; crosshair[3].y = Mouse::getPositionY();
		fgeal::Graphics::drawLine(crosshair[0].x, crosshair[0].y, crosshair[1].x, crosshair[1].y, Color::GREEN);
		fgeal::Graphics::drawLine(crosshair[2].x, crosshair[2].y, crosshair[3].x, crosshair[3].y, Color::GREEN);

		if(outlineOn)
		{
			fgeal::Graphics::drawRectangle(outline, outlineColor);
			fgeal::Graphics::drawLine(outline.x, outline.y, outline.x+outline.w, outline.y+outline.h, outlineColor);
		}

		fgeal::Graphics::drawEllipse(0.5*display.getWidth(), (0.5 + 0.05*knobY)*display.getHeight(), 8, 4, Color::PURPLE);

		float posTxt = 16;
		fontMono->drawText("Mouse test (press F4 again for joystick test)", 16, posTxt+=fontMonoHeight, fgeal::Color::GREEN);
		fontMono->drawText("Mouse cursor position is tracked and show as a cross", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Pressing and holding a mouse button trigger drawing a rectangle; each mouse button gives a different color", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Moving the mouse wheel moves a ellipse on the screen", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);

		fontMono->drawText("Press F1 through F9 keys for other tests", 16, game.getDisplay().getHeight() - fontMonoHeight, fgeal::Color::WHITE);
	}

	void update(float delta)
	{
		if(knobY > 10) knobY = 10;
		else if(knobY < -10) knobY = -10;
	}

	void onKeyPressed(fgeal::Keyboard::Key key)
	{
		globalInputHandler(key, game, *this);
	}

	void onMouseButtonPressed(fgeal::Mouse::Button button, int x, int y)
	{
		outline.x = x;
		outline.y = y;
		outline.w = outline.h = 0;
		outlineOn = true;

		switch(button)
		{
			case fgeal::Mouse::BUTTON_LEFT:   outlineColor = fgeal::Color::BLUE; break;
			case fgeal::Mouse::BUTTON_RIGHT:  outlineColor = fgeal::Color::RED; break;
			case fgeal::Mouse::BUTTON_MIDDLE: outlineColor = fgeal::Color::YELLOW; break;
			default:case fgeal::Mouse::BUTTON_UNKNOWN: outlineColor = fgeal::Color::GREY; break;
		}
	}

	void onMouseButtonReleased(fgeal::Mouse::Button, int x, int y)
	{
		outlineOn = false;
	}

	void onMouseMoved(int x, int y)
	{
		if(outlineOn)
		{
			outline.w = x - outline.x;
			outline.h = y - outline.y;
		}
	}

	void onMouseWheelMoved(int change)
	{
		knobY -= change;
	}
};
