/*
 * digits_test.hxx
 *
 *  Created on: 11/11/2024
 *      Author: hydren
 */

#include <ciso646>

#include <fgeal/fgeal.hpp>
#include <fgeal/extra/game.hpp>
#include <fgeal/extra/gui.hpp>
#include <fgeal/extra/primitives.hpp>

template<typename CharType, void (*drawDigitFunction)(CharType, float, float, float, const fgeal::Color&)>
void debugDrawCharFunction(CharType ch, float x, float y, float s, const fgeal::Color& c)
{
	fgeal::Graphics::drawRectangle(x-1, y-1, 2+0.5f*s, 2+s, fgeal::Color::WHITE);
	drawDigitFunction(ch, x, y, s, c);
}

template<typename CharType, void (*drawDigitFunction)(CharType, float, float, float, const fgeal::Color&)>
void debugDrawCharFunction2(CharType ch, float x, float y, float s, const fgeal::Color& c)
{
	fgeal::Graphics::drawRectangle(x-1, y-1, 2+0.5625f*s, 2+s, fgeal::Color::WHITE);
	drawDigitFunction(ch, x, y, s, c);
}

class DigitsTest : public fgeal::Game::State
{
	fgeal::Button ssdBtnA, ssdBtnB, ssdBtnC, ssdBtnD, ssdBtnE, ssdBtnF, ssdBtnG;
	fgeal::Button fsdBtnA, fsdBtnB, fsdBtnC, fsdBtnD, fsdBtnE, fsdBtnF, fsdBtnG1, fsdBtnG2, fsdBtnH, fsdBtnI, fsdBtnJ, fsdBtnK, fsdBtnL, fsdBtnM;

	fgeal::Point digitsPosition, charsPosition, ssdDigitsPosition, ssdCharsPosition, ssdPosition, fsdDigitsPosition, fsdCharsPosition, fsdPosition;

	enum DrawCharDebugMode { DRAW_CHAR_DEBUG_NONE, DRAW_CHAR_DEBUG_OPAQUE, DRAW_CHAR_DEBUG_TRANSPARENT , DRAW_CHAR_DEBUG_COUNT } debugDrawChar;

	unsigned char ssd;
	unsigned short fsd;

	bool whiteBg;

	public:
	int getId() { return 16; }

	DigitsTest(fgeal::Game& game)
	: State(game), debugDrawChar(), ssd(), fsd(), whiteBg()
	{}

	~DigitsTest()
	{}

	void initialize()
	{
		ssdBtnA.text.setFont(fontMono);
		ssdBtnA.text.setColor(fgeal::Color::MAROON);
		ssdBtnA.text = "segment A";
		ssdBtnA.textHorizontalAlignment = fgeal::Label::ALIGN_LEADING;
		ssdBtnA.backgroundColor = fgeal::Color::RED;
		ssdBtnA.borderColor = fgeal::Color::MAROON;
		ssdBtnA.borderDisabled = false;

		ssdBtnB = ssdBtnA;
		ssdBtnB.text = "segment B";

		ssdBtnC = ssdBtnA;
		ssdBtnC.text = "segment C";

		ssdBtnD = ssdBtnA;
		ssdBtnD.text = "segment D";

		ssdBtnE = ssdBtnA;
		ssdBtnE.text = "segment E";

		ssdBtnF = ssdBtnA;
		ssdBtnF.text = "segment F";

		ssdBtnG = ssdBtnA;
		ssdBtnG.text = "segment G";

		fsdBtnA = ssdBtnA;
		fsdBtnA.text = "segment A ";
		fsdBtnA.text.setColor(fgeal::Color::TEAL);
		fsdBtnA.backgroundColor = fgeal::Color::CYAN;
		fsdBtnA.borderColor = fgeal::Color::TEAL;

		fsdBtnB = fsdBtnA;
		fsdBtnB.text = "segment B";

		fsdBtnC = fsdBtnA;
		fsdBtnC.text = "segment C";

		fsdBtnD = fsdBtnA;
		fsdBtnD.text = "segment D";

		fsdBtnE = fsdBtnA;
		fsdBtnE.text = "segment E";

		fsdBtnF = fsdBtnA;
		fsdBtnF.text = "segment F";

		fsdBtnG1 = fsdBtnA;
		fsdBtnG1.text = "segment G1";

		fsdBtnG2 = fsdBtnA;
		fsdBtnG2.text = "segment G2";

		fsdBtnH = fsdBtnA;
		fsdBtnH.text = "segment H";

		fsdBtnI = fsdBtnA;
		fsdBtnI.text = "segment I";

		fsdBtnJ = fsdBtnA;
		fsdBtnJ.text = "segment J";

		fsdBtnK = fsdBtnA;
		fsdBtnK.text = "segment K";

		fsdBtnL = fsdBtnA;
		fsdBtnL.text = "segment L";

		fsdBtnM = fsdBtnA;
		fsdBtnM.text = "segment M";
	}

	void onEnter()
	{
		const float w = game.getDisplay().getWidth(), h = game.getDisplay().getHeight();

		digitsPosition.x = 0.025*w; digitsPosition.y = 0.25*h;
		charsPosition.x = digitsPosition.x + 150; charsPosition.y = 0.25*h;

		ssdDigitsPosition.x = 0.025*w; ssdDigitsPosition.y = 0.4*h;
		ssdCharsPosition.x = ssdDigitsPosition.x + 150; ssdCharsPosition.y = 0.4*h;
		ssdPosition.x = 0.025*w; ssdPosition.y = ssdDigitsPosition.y + 60;

		ssdBtnA.pack();
		ssdBtnA.bounds.x = ssdPosition.x + 64; ssdBtnA.bounds.y = ssdPosition.y;
		ssdBtnA.backgroundDisabled = true;

		ssdBtnB.bounds = ssdBtnA.bounds;
		ssdBtnB.bounds.y += 16;
		ssdBtnB.backgroundDisabled = true;

		ssdBtnC.bounds = ssdBtnB.bounds;
		ssdBtnC.bounds.y += 16;
		ssdBtnC.backgroundDisabled = true;

		ssdBtnD.bounds = ssdBtnC.bounds;
		ssdBtnD.bounds.y += 16;
		ssdBtnD.backgroundDisabled = true;

		ssdBtnE.bounds = ssdBtnD.bounds;
		ssdBtnE.bounds.y += 16;
		ssdBtnE.backgroundDisabled = true;

		ssdBtnF.bounds = ssdBtnE.bounds;
		ssdBtnF.bounds.y += 16;
		ssdBtnF.backgroundDisabled = true;

		ssdBtnG.bounds = ssdBtnF.bounds;
		ssdBtnG.bounds.y += 16;
		ssdBtnG.backgroundDisabled = true;

		fsdDigitsPosition.x = 0.025*w; fsdDigitsPosition.y = 0.65*h;
		fsdCharsPosition.x = fsdDigitsPosition.x + 150; fsdCharsPosition.y = 0.65*h;
		fsdPosition.x = 0.025*w; fsdPosition.y = fsdDigitsPosition.y + 72;

		fsdBtnA.pack();
		fsdBtnA.bounds.x = fsdPosition.x + 96; fsdBtnA.bounds.y = fsdPosition.y;
		fsdBtnA.backgroundDisabled = true;

		fsdBtnB.bounds = fsdBtnA.bounds;
		fsdBtnB.bounds.y += 16;
		fsdBtnB.backgroundDisabled = true;

		fsdBtnC.bounds = fsdBtnB.bounds;
		fsdBtnC.bounds.y += 16;
		fsdBtnC.backgroundDisabled = true;

		fsdBtnD.bounds = fsdBtnC.bounds;
		fsdBtnD.bounds.y += 16;
		fsdBtnD.backgroundDisabled = true;

		fsdBtnE.bounds = fsdBtnD.bounds;
		fsdBtnE.bounds.y += 16;
		fsdBtnE.backgroundDisabled = true;

		fsdBtnF.bounds = fsdBtnE.bounds;
		fsdBtnF.bounds.y += 16;
		fsdBtnF.backgroundDisabled = true;

		fsdBtnG1.bounds = fsdBtnF.bounds;
		fsdBtnG1.bounds.y += 16;
		fsdBtnG1.backgroundDisabled = true;

		fsdBtnG2.bounds = fsdBtnA.bounds;
		fsdBtnG2.bounds.x += fsdBtnA.bounds.w + 8;
		fsdBtnG2.backgroundDisabled = true;

		fsdBtnH.bounds = fsdBtnG2.bounds;
		fsdBtnH.bounds.y += 16;
		fsdBtnH.backgroundDisabled = true;

		fsdBtnI.bounds = fsdBtnH.bounds;
		fsdBtnI.bounds.y += 16;
		fsdBtnI.backgroundDisabled = true;

		fsdBtnJ.bounds = fsdBtnI.bounds;
		fsdBtnJ.bounds.y += 16;
		fsdBtnJ.backgroundDisabled = true;

		fsdBtnK.bounds = fsdBtnJ.bounds;
		fsdBtnK.bounds.y += 16;
		fsdBtnK.backgroundDisabled = true;

		fsdBtnL.bounds = fsdBtnK.bounds;
		fsdBtnL.bounds.y += 16;
		fsdBtnL.backgroundDisabled = true;

		fsdBtnM.bounds = fsdBtnL.bounds;
		fsdBtnM.bounds.y += 16;
		fsdBtnM.backgroundDisabled = true;

		debugDrawChar = DRAW_CHAR_DEBUG_NONE;
		fsd = ssd = 0;
	}

	void onLeave()
	{}

	void render()
	{
		fgeal::Display& display = game.getDisplay();
		display.clear();
		if(whiteBg) fgeal::Graphics::drawFilledRectangle(0, 0, display.getWidth(), display.getHeight());

		static const std::string testString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_='\"?,*<>", extraTestString = "abcdefghijklmnopqrstuvwxyz";
		if(not debugDrawChar)
		{
			fgeal::primitives::drawDigits(12345, digitsPosition.x, digitsPosition.y, 16, fgeal::Color::CHARTREUSE);
			fgeal::primitives::drawDigits(67890, digitsPosition.x, digitsPosition.y+20, 32, fgeal::Color::MAGENTA);
			fgeal::primitives::drawCustomDigits<fgeal::primitives::drawSevenSegmentDigit>(12345, ssdDigitsPosition.x, ssdDigitsPosition.y, 16, fgeal::Color::GREEN);
			fgeal::primitives::drawCustomDigits<fgeal::primitives::drawSevenSegmentDigit>(67890, ssdDigitsPosition.x, ssdDigitsPosition.y+20, 32, fgeal::Color::ORANGE);
			fgeal::primitives::drawCustomDigits<fgeal::primitives::drawFourteenSegmentDigit>(12345, fsdDigitsPosition.x, fsdDigitsPosition.y, 16, fgeal::Color::SPRING_GREEN);
			fgeal::primitives::drawCustomDigits<fgeal::primitives::drawFourteenSegmentDigit>(67890, fsdDigitsPosition.x, fsdDigitsPosition.y+20, 32, fgeal::Color::ROSE);
			fgeal::primitives::drawString(testString, charsPosition.x, charsPosition.y, 20, 15, fgeal::Color::VIOLET);
			fgeal::primitives::drawCustomCharacters<fgeal::primitives::drawSevenSegmentCharacter>(testString.c_str(), testString.length(), ssdCharsPosition.x, ssdCharsPosition.y, 20, 15, fgeal::Color::CYAN);
			fgeal::primitives::drawCustomCharacters<fgeal::primitives::drawFourteenSegmentCharacter>(testString.c_str(), testString.length(), fsdCharsPosition.x, fsdCharsPosition.y, 20, 15, fgeal::Color::AZURE);
			fgeal::primitives::drawCustomCharacters<fgeal::primitives::drawFourteenSegmentCharacter>(extraTestString.c_str(), extraTestString.length(), fsdCharsPosition.x, fsdCharsPosition.y+24, 20, 15, fgeal::Color::AZURE);
		}
		else
		{
			const unsigned char a = debugDrawChar == DRAW_CHAR_DEBUG_TRANSPARENT? 127 : 255;
			fgeal::primitives::drawCustomDigits< debugDrawCharFunction<unsigned char, fgeal::primitives::drawDigit> >(12345, digitsPosition.x, digitsPosition.y, 16, fgeal::Color::CHARTREUSE.getTransparent(a));
			fgeal::primitives::drawCustomDigits< debugDrawCharFunction<unsigned char, fgeal::primitives::drawDigit> >(67890, digitsPosition.x, digitsPosition.y+20, 32, fgeal::Color::MAGENTA.getTransparent(a));
			fgeal::primitives::drawCustomDigits< debugDrawCharFunction2<unsigned char, fgeal::primitives::drawSevenSegmentDigit> >(12345, ssdDigitsPosition.x, ssdDigitsPosition.y, 16, fgeal::Color::GREEN.getTransparent(a));
			fgeal::primitives::drawCustomDigits< debugDrawCharFunction2<unsigned char, fgeal::primitives::drawSevenSegmentDigit> >(67890, ssdDigitsPosition.x, ssdDigitsPosition.y+20, 32, fgeal::Color::ORANGE.getTransparent(a));
			fgeal::primitives::drawCustomDigits< debugDrawCharFunction2<unsigned char, fgeal::primitives::drawFourteenSegmentDigit> >(12345, fsdDigitsPosition.x, fsdDigitsPosition.y, 16, fgeal::Color::SPRING_GREEN.getTransparent(a));
			fgeal::primitives::drawCustomDigits< debugDrawCharFunction2<unsigned char, fgeal::primitives::drawFourteenSegmentDigit> >(67890, fsdDigitsPosition.x, fsdDigitsPosition.y+20, 32, fgeal::Color::ROSE.getTransparent(a));
			fgeal::primitives::drawCustomCharacters< debugDrawCharFunction<char, fgeal::primitives::drawCharacter> >(testString.c_str(), testString.length(), charsPosition.x, charsPosition.y, 20, 15, fgeal::Color::VIOLET.getTransparent(a));
			fgeal::primitives::drawCustomCharacters< debugDrawCharFunction2<char, fgeal::primitives::drawSevenSegmentCharacter> >(testString.c_str(), testString.length(), ssdCharsPosition.x, ssdCharsPosition.y, 20, 15, fgeal::Color::CYAN.getTransparent(a));
			fgeal::primitives::drawCustomCharacters< debugDrawCharFunction2<char, fgeal::primitives::drawFourteenSegmentCharacter> >(testString.c_str(), testString.length(), fsdCharsPosition.x, fsdCharsPosition.y, 20, 15, fgeal::Color::AZURE.getTransparent(a));
			fgeal::primitives::drawCustomCharacters< debugDrawCharFunction2<char, fgeal::primitives::drawFourteenSegmentCharacter> >(extraTestString.c_str(), extraTestString.length(), fsdCharsPosition.x, fsdCharsPosition.y+24, 20, 15, fgeal::Color::AZURE.getTransparent(a));
		}

		const static unsigned char fullSsd = fgeal::primitives::SSD_SEGMENT_A | fgeal::primitives::SSD_SEGMENT_B | fgeal::primitives::SSD_SEGMENT_C | fgeal::primitives::SSD_SEGMENT_D
										   | fgeal::primitives::SSD_SEGMENT_E | fgeal::primitives::SSD_SEGMENT_F | fgeal::primitives::SSD_SEGMENT_G;
		if(debugDrawChar != DRAW_CHAR_DEBUG_TRANSPARENT)
			fgeal::primitives::drawSevenSegmentDisplay(fullSsd, ssdPosition.x, ssdPosition.y, 64, fgeal::Color::RED.getTransparent(64));
		fgeal::primitives::drawSevenSegmentDisplay(ssd, ssdPosition.x, ssdPosition.y, 64, fgeal::Color::RED.getTransparent(debugDrawChar == DRAW_CHAR_DEBUG_TRANSPARENT? 127 : 255));

		ssdBtnA.draw();
		ssdBtnB.draw();
		ssdBtnC.draw();
		ssdBtnD.draw();
		ssdBtnE.draw();
		ssdBtnF.draw();
		ssdBtnG.draw();

		const static unsigned short fullFsd = fgeal::primitives::FSD_SEGMENT_A | fgeal::primitives::FSD_SEGMENT_B | fgeal::primitives::FSD_SEGMENT_C | fgeal::primitives::FSD_SEGMENT_D
											| fgeal::primitives::FSD_SEGMENT_E | fgeal::primitives::FSD_SEGMENT_F | fgeal::primitives::FSD_SEGMENT_G1| fgeal::primitives::FSD_SEGMENT_G2
											| fgeal::primitives::FSD_SEGMENT_H | fgeal::primitives::FSD_SEGMENT_I | fgeal::primitives::FSD_SEGMENT_J | fgeal::primitives::FSD_SEGMENT_K
											| fgeal::primitives::FSD_SEGMENT_L | fgeal::primitives::FSD_SEGMENT_M;
		if(debugDrawChar != DRAW_CHAR_DEBUG_TRANSPARENT)
			fgeal::primitives::drawFourteenSegmentDisplay(fullFsd, fsdPosition.x, fsdPosition.y, 72, fgeal::Color::CYAN.getTransparent(48));
		fgeal::primitives::drawFourteenSegmentDisplay(fsd, fsdPosition.x, fsdPosition.y, 72, fgeal::Color::CYAN.getTransparent(debugDrawChar == DRAW_CHAR_DEBUG_TRANSPARENT? 127 : 255));

		fsdBtnA.draw();
		fsdBtnB.draw();
		fsdBtnC.draw();
		fsdBtnD.draw();
		fsdBtnE.draw();
		fsdBtnF.draw();
		fsdBtnG1.draw();
		fsdBtnG2.draw();
		fsdBtnH.draw();
		fsdBtnI.draw();
		fsdBtnJ.draw();
		fsdBtnK.draw();
		fsdBtnL.draw();
		fsdBtnM.draw();

		const fgeal::Color textColor = whiteBg? fgeal::Color::BLACK : fgeal::Color::WHITE;
		float posTxt = 16;
		fontMono->drawText("Digits test (press F3 again for font test)", 16, posTxt+=fontMonoHeight, fgeal::Color::GREEN);
		fontMono->drawText("Press space to switch the background from white/black", 16, posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press alt to toogle drawing of the digits bounds", 16, posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Click on segments' buttons to lit them up", 16, posTxt+=fontMonoHeight, textColor);

		fontMono->drawText("Press F1 through F9 keys for other tests", 16, game.getDisplay().getHeight() - fontMonoHeight, textColor);
	}

	void update(float delta)
	{}

	void onKeyPressed(fgeal::Keyboard::Key key)
	{
		globalInputHandler(key, game, *this);

		if(key == fgeal::Keyboard::KEY_SPACE)
			whiteBg = !whiteBg;

		if(key == fgeal::Keyboard::KEY_LEFT_ALT or key == fgeal::Keyboard::KEY_RIGHT_ALT)
			debugDrawChar = static_cast<DrawCharDebugMode>((debugDrawChar+1) % DRAW_CHAR_DEBUG_COUNT);
	}

	void onMouseButtonPressed(fgeal::Mouse::Button btn, int x, int y)
	{
		if(btn == fgeal::Mouse::BUTTON_LEFT)
		{
			if(ssdBtnA.bounds.contains(x, y))
			{
				if(ssd & fgeal::primitives::SSD_SEGMENT_A) ssd &= ~fgeal::primitives::SSD_SEGMENT_A;
													  else ssd |=  fgeal::primitives::SSD_SEGMENT_A;
				ssdBtnA.backgroundDisabled = not (ssd & fgeal::primitives::SSD_SEGMENT_A);
			}
			if(ssdBtnB.bounds.contains(x, y))
			{
				if(ssd & fgeal::primitives::SSD_SEGMENT_B) ssd &= ~fgeal::primitives::SSD_SEGMENT_B;
													  else ssd |=  fgeal::primitives::SSD_SEGMENT_B;
				ssdBtnB.backgroundDisabled = not (ssd & fgeal::primitives::SSD_SEGMENT_B);
			}
			if(ssdBtnC.bounds.contains(x, y))
			{
				if(ssd & fgeal::primitives::SSD_SEGMENT_C) ssd &= ~fgeal::primitives::SSD_SEGMENT_C;
													  else ssd |=  fgeal::primitives::SSD_SEGMENT_C;
				ssdBtnC.backgroundDisabled = not (ssd & fgeal::primitives::SSD_SEGMENT_C);
			}
			if(ssdBtnD.bounds.contains(x, y))
			{
				if(ssd & fgeal::primitives::SSD_SEGMENT_D) ssd &= ~fgeal::primitives::SSD_SEGMENT_D;
													  else ssd |=  fgeal::primitives::SSD_SEGMENT_D;
				ssdBtnD.backgroundDisabled = not (ssd & fgeal::primitives::SSD_SEGMENT_D);
			}
			if(ssdBtnE.bounds.contains(x, y))
			{
				if(ssd & fgeal::primitives::SSD_SEGMENT_E) ssd &= ~fgeal::primitives::SSD_SEGMENT_E;
													  else ssd |=  fgeal::primitives::SSD_SEGMENT_E;
				ssdBtnE.backgroundDisabled = not (ssd & fgeal::primitives::SSD_SEGMENT_E);
			}
			if(ssdBtnF.bounds.contains(x, y))
			{
				if(ssd & fgeal::primitives::SSD_SEGMENT_F) ssd &= ~fgeal::primitives::SSD_SEGMENT_F;
													  else ssd |=  fgeal::primitives::SSD_SEGMENT_F;
				ssdBtnF.backgroundDisabled = not (ssd & fgeal::primitives::SSD_SEGMENT_F);
			}
			if(ssdBtnG.bounds.contains(x, y))
			{
				if(ssd & fgeal::primitives::SSD_SEGMENT_G) ssd &= ~fgeal::primitives::SSD_SEGMENT_G;
													  else ssd |=  fgeal::primitives::SSD_SEGMENT_G;
				ssdBtnG.backgroundDisabled = not (ssd & fgeal::primitives::SSD_SEGMENT_G);
			}

			if(fsdBtnA.bounds.contains(x, y))
			{
				if(fsd & fgeal::primitives::FSD_SEGMENT_A) fsd &= ~fgeal::primitives::FSD_SEGMENT_A;
													  else fsd |=  fgeal::primitives::FSD_SEGMENT_A;
				fsdBtnA.backgroundDisabled = not (fsd & fgeal::primitives::FSD_SEGMENT_A);
			}
			if(fsdBtnB.bounds.contains(x, y))
			{
				if(fsd & fgeal::primitives::FSD_SEGMENT_B) fsd &= ~fgeal::primitives::FSD_SEGMENT_B;
													  else fsd |=  fgeal::primitives::FSD_SEGMENT_B;
				fsdBtnB.backgroundDisabled = not (fsd & fgeal::primitives::FSD_SEGMENT_B);
			}
			if(fsdBtnC.bounds.contains(x, y))
			{
				if(fsd & fgeal::primitives::FSD_SEGMENT_C) fsd &= ~fgeal::primitives::FSD_SEGMENT_C;
													  else fsd |=  fgeal::primitives::FSD_SEGMENT_C;
				fsdBtnC.backgroundDisabled = not (fsd & fgeal::primitives::FSD_SEGMENT_C);
			}
			if(fsdBtnD.bounds.contains(x, y))
			{
				if(fsd & fgeal::primitives::FSD_SEGMENT_D) fsd &= ~fgeal::primitives::FSD_SEGMENT_D;
													  else fsd |=  fgeal::primitives::FSD_SEGMENT_D;
				fsdBtnD.backgroundDisabled = not (fsd & fgeal::primitives::FSD_SEGMENT_D);
			}
			if(fsdBtnE.bounds.contains(x, y))
			{
				if(fsd & fgeal::primitives::FSD_SEGMENT_E) fsd &= ~fgeal::primitives::FSD_SEGMENT_E;
													  else fsd |=  fgeal::primitives::FSD_SEGMENT_E;
				fsdBtnE.backgroundDisabled = not (fsd & fgeal::primitives::FSD_SEGMENT_E);
			}
			if(fsdBtnF.bounds.contains(x, y))
			{
				if(fsd & fgeal::primitives::FSD_SEGMENT_F) fsd &= ~fgeal::primitives::FSD_SEGMENT_F;
													  else fsd |=  fgeal::primitives::FSD_SEGMENT_F;
				fsdBtnF.backgroundDisabled = not (fsd & fgeal::primitives::FSD_SEGMENT_F);
			}
			if(fsdBtnG1.bounds.contains(x, y))
			{
				if(fsd & fgeal::primitives::FSD_SEGMENT_G1) fsd &= ~fgeal::primitives::FSD_SEGMENT_G1;
													   else fsd |=  fgeal::primitives::FSD_SEGMENT_G1;
				fsdBtnG1.backgroundDisabled = not (fsd & fgeal::primitives::FSD_SEGMENT_G1);
			}
			if(fsdBtnG2.bounds.contains(x, y))
			{
				if(fsd & fgeal::primitives::FSD_SEGMENT_G2) fsd &= ~fgeal::primitives::FSD_SEGMENT_G2;
													   else fsd |=  fgeal::primitives::FSD_SEGMENT_G2;
				fsdBtnG2.backgroundDisabled = not (fsd & fgeal::primitives::FSD_SEGMENT_G2);
			}
			if(fsdBtnH.bounds.contains(x, y))
			{
				if(fsd & fgeal::primitives::FSD_SEGMENT_H) fsd &= ~fgeal::primitives::FSD_SEGMENT_H;
													  else fsd |=  fgeal::primitives::FSD_SEGMENT_H;
				fsdBtnH.backgroundDisabled = not (fsd & fgeal::primitives::FSD_SEGMENT_H);
			}
			if(fsdBtnI.bounds.contains(x, y))
			{
				if(fsd & fgeal::primitives::FSD_SEGMENT_I) fsd &= ~fgeal::primitives::FSD_SEGMENT_I;
													  else fsd |=  fgeal::primitives::FSD_SEGMENT_I;
				fsdBtnI.backgroundDisabled = not (fsd & fgeal::primitives::FSD_SEGMENT_I);
			}
			if(fsdBtnJ.bounds.contains(x, y))
			{
				if(fsd & fgeal::primitives::FSD_SEGMENT_J) fsd &= ~fgeal::primitives::FSD_SEGMENT_J;
													  else fsd |=  fgeal::primitives::FSD_SEGMENT_J;
				fsdBtnJ.backgroundDisabled = not (fsd & fgeal::primitives::FSD_SEGMENT_J);
			}
			if(fsdBtnK.bounds.contains(x, y))
			{
				if(fsd & fgeal::primitives::FSD_SEGMENT_K) fsd &= ~fgeal::primitives::FSD_SEGMENT_K;
													  else fsd |=  fgeal::primitives::FSD_SEGMENT_K;
				fsdBtnK.backgroundDisabled = not (fsd & fgeal::primitives::FSD_SEGMENT_K);
			}
			if(fsdBtnL.bounds.contains(x, y))
			{
				if(fsd & fgeal::primitives::FSD_SEGMENT_L) fsd &= ~fgeal::primitives::FSD_SEGMENT_L;
													  else fsd |=  fgeal::primitives::FSD_SEGMENT_L;
				fsdBtnL.backgroundDisabled = not (fsd & fgeal::primitives::FSD_SEGMENT_L);
			}
			if(fsdBtnM.bounds.contains(x, y))
			{
				if(fsd & fgeal::primitives::FSD_SEGMENT_M) fsd &= ~fgeal::primitives::FSD_SEGMENT_M;
													  else fsd |=  fgeal::primitives::FSD_SEGMENT_M;
				fsdBtnM.backgroundDisabled = not (fsd & fgeal::primitives::FSD_SEGMENT_M);
			}
		}
	}
};
