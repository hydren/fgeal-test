/*
 * font_test.hxx
 *
 *  Created on: 17 de fev de 2017
 *      Author: carlosfaruolo
 */

#include <ciso646>
#include <string>
#include <sstream>
#include <cmath>

#include <fgeal/fgeal.hpp>
#include <fgeal/extra/game.hpp>
#include <fgeal/extra/cached_drawable_text.hpp>

class FontTestState : public fgeal::Game::State
{
	fgeal::Font *fontSans, *fontSerif;
	fgeal::DrawableText* drawableText;
	fgeal::CachedDrawableText* cachedDrawableText;
	float posx, posy;
	std::string text;
	int caret;
	bool showBounds;

	public:
	int getId() { return 6; }

	FontTestState(fgeal::Game& game)
	: State(game),
	  fontSans(null), fontSerif(null), drawableText(null), cachedDrawableText(null),
	  posx(), posy(), text(), caret(0), showBounds(false)
	{}

	~FontTestState()
	{
		delete fontSans;
		delete fontSerif;
	}

	void initialize()
	{
		fontSans = new fgeal::Font("freesans.ttf");
		fontSerif = new fgeal::Font("freeserif.ttf");
		drawableText = new fgeal::DrawableText("test", fontSans, fgeal::Color::ORANGE);
		cachedDrawableText = new fgeal::CachedDrawableText("test", fontSerif, fgeal::Color::ROSE);
	}

	void onEnter()
	{
		fgeal::Display& display = game.getDisplay();
		posx = display.getWidth() * 0.25;
		posy = display.getHeight() * 0.25;
		text = "Type something...";
		drawableText->setContent(text);
		cachedDrawableText->setContent(text);
		caret = text.size();
		showBounds = false;
	}

	void onLeave() {}

	void render()
	{
		using fgeal::Color;
		fgeal::Display& display = game.getDisplay();
		display.clear();

		if(cos(16*fgeal::uptime()) > 0)
			fgeal::Graphics::drawFilledRectangle(posx + fontMono->getTextWidth(text.substr(0, caret)), posy, 2, fontMonoHeight, Color::WHITE);
		if(not text.empty())
		{
			fontMono->drawText(text, posx, posy, Color::WHITE);
			fontSans->drawText(text, 0, posy*1.5, Color::RED);
			fontSerif->drawText(text, display.getWidth() - fontSerif->getTextWidth(text), posy*2, Color::BLUE);
			drawableText->draw(posx, posy*2.5);
			cachedDrawableText->draw(posx, posy*3);

			if(showBounds)
			{
				fgeal::Graphics::drawRectangle(posx, posy, fontMono->getTextWidth(text), fontMonoHeight, Color::WHITE);
				fgeal::Graphics::drawRectangle(0, posy*1.5, fontSans->getTextWidth(text), fontSans->getTextHeight(), Color::RED);
				fgeal::Graphics::drawRectangle(display.getWidth() - fontSerif->getTextWidth(text), posy*2.0, fontSerif->getTextWidth(text), fontSerif->getTextHeight(), Color::BLUE);
				fgeal::Graphics::drawRectangle(posx, posy*2.5, drawableText->getWidth(), drawableText->getHeight(), Color::ORANGE);
				fgeal::Graphics::drawRectangle(posx, posy*3, cachedDrawableText->getWidth(), cachedDrawableText->getHeight(), Color::ROSE);
			}
		}

		float posTxt = 16;
		fontMono->drawText("Font test (press F3 again for digits test)", 16, posTxt+=fontMonoHeight, fgeal::Color::GREEN);
		fontMono->drawText("Type characters to edit the string shown; pressing backspace delete character at caret position.", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Left and right arrow keys move the caret; holding shift keys should give uppercase versions of each key", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Press left or right control keys to toggle show text bounds.", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Press numpad plus and minus keys to increase and decrease font size", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Press numpad multiplication key to exchange cached and non-cached drawable text fonts", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);

		fontMono->drawText("Press F1 through F9 keys for other tests", 16, game.getDisplay().getHeight() - fontMonoHeight, fgeal::Color::WHITE);
	}

	void update(float delta)
	{}

	void onKeyPressed(fgeal::Keyboard::Key key)
	{
		globalInputHandler(key, game, *this);

		bool erase = false;
		using fgeal::Keyboard;
		switch(key)
		{
			case Keyboard::KEY_LEFT_CONTROL:
			case Keyboard::KEY_RIGHT_CONTROL:
				showBounds = !showBounds;
				break;

				// caret
			case Keyboard::KEY_ARROW_LEFT:
				if(caret > 0) caret--;
				break;

			case Keyboard::KEY_ARROW_RIGHT:
				if(caret < (int) text.size()) caret++;
				break;

			case Keyboard::KEY_BACKSPACE:
				erase = true;
				break;

			case Keyboard::KEY_NUMPAD_ADDITION:
				fontSans->setSize(fontSans->getSize()+2);
				fontSerif->setSize(fontSerif->getSize()+2);
				break;

			case Keyboard::KEY_NUMPAD_SUBTRACTION:
				fontSans->setSize(fontSans->getSize()-2);
				fontSerif->setSize(fontSerif->getSize()-2);
				break;

			case Keyboard::KEY_NUMPAD_MULTIPLICATION:
				static bool fontExchanged = false;
				drawableText->setFont(fontExchanged? fontSans : fontSerif);
				cachedDrawableText->setFont(fontExchanged? fontSerif : fontSans);
				fontExchanged = !fontExchanged;
				break;

			default: break;
		}

		const char typed = Keyboard::parseKeyStroke(key);

		if(typed != '\n')
		{
			text.insert(text.begin() + caret, 1, typed);
			drawableText->setContent(text);
			cachedDrawableText->setContent(text);
			caret++;
		}

		if(erase and not text.empty() and caret > 0)
		{
			text.erase(text.begin() + caret-1);
			drawableText->setContent(text);
			cachedDrawableText->setContent(text);
			caret--;
		}
	}
};
