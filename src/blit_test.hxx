/*
 * blit_test.hxx
 *
 *  Created on: 17 de jan de 2018
 *      Author: carlosfaruolo
 */

#include <fgeal/fgeal.hpp>
#include <fgeal/extra/game.hpp>
#include <fgeal/extra/sprite.hpp>

template <typename T> std::string toString4(const T& t) { std::ostringstream os; os.precision(4); os<<t; return os.str(); }

class BlitTestState : public fgeal::Game::State
{
	fgeal::Image* imgPaper, *imgBrush, *imgStamp;
	fgeal::Font* bigFont, *font;
	fgeal::Point center, target;
	fgeal::Rectangle region;
	fgeal::Vector2D scale;
	fgeal::Image::FlipMode flipmode;
	fgeal::Color queriedColor;
	float angle;
	bool whiteBg, useSetTarget, translucentDrawing;

	public:
	int getId() { return 3; }

	BlitTestState(fgeal::Game& game)
	: State(game),
	  imgPaper(null), imgBrush(null), imgStamp(null), bigFont(null), font(null),
	  center(), target(), region(), scale(), flipmode(), angle(), whiteBg(), useSetTarget(), translucentDrawing()
	{}

	~BlitTestState()
	{
		delete imgPaper;
		delete imgBrush;
		delete imgStamp;
		delete bigFont;
	}

	void initialize()
	{
		imgBrush = new fgeal::Image("icon.png");
		imgStamp = new fgeal::Image("stamp.jpg");
		imgPaper = new fgeal::Image(imgBrush->getWidth(), imgBrush->getHeight());
		bigFont = new fgeal::Font("freemono.ttf", 54);
	}

	void onEnter()
	{
		scale.x = scale.y = 1.0;
		angle = 0;
		center.x = 0.5*imgBrush->getWidth();
		center.y = 0.5*imgBrush->getHeight();
		flipmode = fgeal::Image::FLIP_NONE;
		region.x = region.y = 0;
		target.x = target.y = 0;
		region.w = imgBrush->getWidth();
		region.h = imgBrush->getHeight();
		translucentDrawing = whiteBg = useSetTarget = false;
		font = fontMono;
	}

	void onLeave() {}

	void render()
	{
		fgeal::Display& display = game.getDisplay();
		const fgeal::Point posBrush = { 0.5f*imgBrush->getWidth(), 0.5f*imgBrush->getHeight() },
						   posPaper = { (float)display.getWidth() - imgPaper->getWidth(), (float)display.getHeight() - imgPaper->getHeight()};
		display.clear();
		if(whiteBg) fgeal::Graphics::drawFilledRectangle(0, 0, game.getDisplay().getWidth(), game.getDisplay().getHeight());

		imgBrush->drawScaledRotatedRegion(posBrush, scale, angle, center, flipmode, region);

		imgPaper->draw(posPaper);
		fgeal::Graphics::drawRectangle(posPaper.x, posPaper.y, imgPaper->getWidth(), imgPaper->getHeight(), fgeal::Color::RED);
		fgeal::Graphics::drawCircle(posPaper.x + target.x, posPaper.y + target.y, 4, fgeal::Color::RED);

		const fgeal::Color textColor = whiteBg? fgeal::Color::BLACK : fgeal::Color::WHITE;
		float posTxt = display.getHeight() - 2*fontMonoHeight;
		fontMono->drawText("x scale:"  + toString4(scale.x),  16, posTxt-=fontMonoHeight, textColor);
		fontMono->drawText("y scale:"  + toString4(scale.y),  16, posTxt-=fontMonoHeight, textColor);
		fontMono->drawText("angle:"    + toString4(angle),    16, posTxt-=fontMonoHeight, textColor);
		fontMono->drawText("x center:" + toString4(center.x), 16, posTxt-=fontMonoHeight, textColor);
		fontMono->drawText("y center:" + toString4(center.y), 16, posTxt-=fontMonoHeight, textColor);
		fontMono->drawText("flipmode:" + toString4(flipmode), 16, posTxt-=fontMonoHeight, textColor);
		fontMono->drawText("x region:" + toString4(region.x), 16, posTxt-=fontMonoHeight, textColor);
		fontMono->drawText("y region:" + toString4(region.y), 16, posTxt-=fontMonoHeight, textColor);
		fontMono->drawText("w region:" + toString4(region.w), 16, posTxt-=fontMonoHeight, textColor);
		fontMono->drawText("h region:" + toString4(region.h), 16, posTxt-=fontMonoHeight, textColor);
		fontMono->drawText(string("using ") + (translucentDrawing? "translucent" : "opaque") + " color to draw circle/text", 16, posTxt-=fontMonoHeight, textColor);
		fontMono->drawText(string("using ") + (font==fontMono? "small font" : "big font"), 16, posTxt-=fontMonoHeight, textColor);
		fontMono->drawText(string("using ") + (useSetTarget? "set draw target" : "blit method"), 16, posTxt-=fontMonoHeight, textColor);

		fgeal::Graphics::drawFilledRectangle(16, posTxt-=2*fontMonoHeight, 0.02*display.getWidth(), 0.02*display.getWidth(), queriedColor);
		fgeal::Graphics::drawRectangle(16, posTxt, 0.02*display.getWidth(), 0.02*display.getWidth(), textColor);
		fontMono->drawText(queriedColor.toRgbaString(), 16 + 0.025*display.getWidth(), posTxt, textColor);

		posTxt = 16;
		fontMono->drawText("Blitting test (press F1 again for image drawing test)", 0.5*display.getWidth(), posTxt+=fontMonoHeight, fgeal::Color::GREEN);
		fontMono->drawText("Press enter key to blit logo", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press enter backspace to blit stamp", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Arrow keys controls source rectangle position", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Keypad arrow keys controls destination position", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("A-Q keys controls y-scale", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Z-X keys controls x-scale", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("W-E keys controls both x-scale and y-scale", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press R-T keys to changle angle of rotation", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press F key to flipping mode", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press space to switch the background from white/black", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press alt key to clear blit target", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press control key to switch between draw target mode and blit method mode", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press shift key to switch between small font and big font when blitting the \"Ok!\" text", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press tab key to switch between draw translucent color when blitting circle/text", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press I key to put a single azure pixel", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press O key to blit a orange circle", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press P key to blit an \"Ok!\" text", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press K key to put chartreuse pixels in a squared region", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press U key to read a single pixel color", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press J key to read pixels colors (mean) in a squared region", 0.5*display.getWidth(), posTxt+=fontMonoHeight, textColor);

		fontMono->drawText("Press F1 through F9 keys for other tests", 16, game.getDisplay().getHeight() - fontMonoHeight, textColor);
	}

	void update(float delta)
	{
		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_A))
			scale.y = scale.y/(1+delta);
		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_Q))
			scale.y = scale.y*(2-1/(1+delta));

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_Z))
			scale.x = scale.x/(1+delta);
		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_X))
			scale.x = scale.x*(2-1/(1+delta));

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_W))
		{	scale.x = scale.x/(1+delta); scale.y = scale.y/(1+delta); }
		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_E))
		{	scale.x = scale.x*(2-1/(1+delta)); scale.y = scale.y*(2-1/(1+delta)); }

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_R))
			angle += 0.5*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_T))
			angle -= 0.5*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_UP))
			region.y -= 150*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_DOWN))
			region.y += 150*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_LEFT))
			region.x -= 150*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_RIGHT))
			region.x += 150*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_PAGE_DOWN))
			region.h -= 150*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_PAGE_UP))
			region.h += 150*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_END))
			region.w -= 150*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_HOME))
			region.w += 150*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_NUMPAD_8))
			target.y -= 150*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_NUMPAD_2))
			target.y += 150*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_NUMPAD_4))
			target.x -= 150*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_NUMPAD_6))
			target.x += 150*delta;
	}

	void onKeyPressed(fgeal::Keyboard::Key key)
	{
		globalInputHandler(key, game, *this);

		if(key == fgeal::Keyboard::KEY_F)
		{
			if(flipmode == fgeal::Image::FLIP_NONE)
				flipmode = fgeal::Image::FLIP_HORIZONTAL;
			else if(flipmode == fgeal::Image::FLIP_HORIZONTAL)
				flipmode = fgeal::Image::FLIP_VERTICAL;
			else if(flipmode == fgeal::Image::FLIP_VERTICAL)
				flipmode = fgeal::Image::FLIP_NONE;
		}

		if(key == fgeal::Keyboard::KEY_ENTER)
		{
			if(useSetTarget)
			{
				fgeal::Graphics::setDrawTarget(imgPaper);
				imgBrush->drawScaledRotatedRegion(target, scale, angle, center, flipmode, region);
				fgeal::Graphics::setDefaultDrawTarget();
			}
			else
				imgBrush->blit(*imgPaper, target, scale, angle, center, flipmode, region);
		}

		if(key == fgeal::Keyboard::KEY_BACKSPACE)
		{
			if(useSetTarget)
			{
				fgeal::Graphics::setDrawTarget(imgPaper);
				imgStamp->drawScaledRotatedRegion(target, scale, angle, center, flipmode, region);
				fgeal::Graphics::setDefaultDrawTarget();
			}
			else
				imgStamp->blit(*imgPaper, target, scale, angle, center, flipmode, region);
		}

		if(key == fgeal::Keyboard::KEY_SPACE)
			whiteBg = !whiteBg;

		if(key == fgeal::Keyboard::KEY_LEFT_ALT or key == fgeal::Keyboard::KEY_RIGHT_ALT)
		{
			delete imgPaper;
			imgPaper = new fgeal::Image(imgBrush->getWidth(), imgBrush->getHeight());
		}

		if(key == fgeal::Keyboard::KEY_TAB)
			translucentDrawing = !translucentDrawing;

		if(key == fgeal::Keyboard::KEY_LEFT_CONTROL or key == fgeal::Keyboard::KEY_RIGHT_CONTROL)
			useSetTarget = !useSetTarget;

		if(key == fgeal::Keyboard::KEY_LEFT_SHIFT or key == fgeal::Keyboard::KEY_RIGHT_SHIFT)
			font = font == fontMono? bigFont : fontMono;

		if(key == fgeal::Keyboard::KEY_I)
			imgPaper->setPixel(target.x, target.y, fgeal::Color::create(64, 192, 240, 255));

		if(key == fgeal::Keyboard::KEY_K)
		{
			std::vector<std::vector<fgeal::Color> > pixelData;
			for(unsigned i = 0; i < 16; i++)
			{
				pixelData.resize(pixelData.size()+1);
				for(unsigned j = 0; j < 16; j++)
					pixelData[i].push_back(fgeal::Color::CHARTREUSE);
			}
			imgPaper->setPixels(target.x, target.y, pixelData);
		}

		if(key == fgeal::Keyboard::KEY_U)
			queriedColor = imgPaper->getPixel(target.x, target.y);

		if(key == fgeal::Keyboard::KEY_J)
		{
			double r=0, g=0, b=0, a=0;
			std::vector<std::vector<fgeal::Color> > pixelData;
			imgPaper->getPixels(pixelData, target.x, target.y, 16, 16);
			for(unsigned i = 0, n = 1; i < pixelData.size();    i++)
			for(unsigned j = 0;        j < pixelData[i].size(); j++, n++)
			{
				r += (pixelData[i][j].r - r) / n;
				g += (pixelData[i][j].g - g) / n;
				b += (pixelData[i][j].b - b) / n;
				a += (pixelData[i][j].a - a) / n;
			}
			queriedColor.r = r;
			queriedColor.g = g;
			queriedColor.b = b;
			queriedColor.a = a;
		}

		if(key == fgeal::Keyboard::KEY_O)
		{
			fgeal::Graphics::setDrawTarget(imgPaper);
			fgeal::Graphics::drawFilledCircle(target, 16, translucentDrawing? fgeal::Color::ORANGE.getTransparent() : fgeal::Color::ORANGE);
			fgeal::Graphics::setDefaultDrawTarget();
		}

		if(key == fgeal::Keyboard::KEY_P)
		{
			fgeal::Graphics::setDrawTarget(imgPaper);
			font->drawText("Ok!", target.x, target.y, translucentDrawing? fgeal::Color::ORANGE.getTransparent() : fgeal::Color::ORANGE);
			fgeal::Graphics::setDefaultDrawTarget();
		}
	}
};
