/*
 * file_test.hxx
 *
 *  Created on: 20 de abr de 2017
 *      Author: carlosfaruolo
 */

#include <ciso646>

#include <string>
#include <vector>

#include <fgeal/fgeal.hpp>
#include <fgeal/exceptions.hpp>
#include <fgeal/extra/game.hpp>

class FileTestState : public fgeal::Game::State
{
	fgeal::Font *fontSans;

	std::string currentPath;
	std::vector<std::string> fileList;
	std::vector<std::string> folderList;

	unsigned cursor;

	public:
	int getId() { return 12; }

	FileTestState(fgeal::Game& game)
	: State(game),
	  fontSans(null),
	  cursor(0)
	{}

	~FileTestState()
	{
		delete fontSans;
	}

	void initialize()
	{
		fontSans = new fgeal::Font("freesans.ttf");
	}

	void onEnter()
	{
		this->navigate(fgeal::filesystem::getCurrentWorkingDirectory());
	}

	void onLeave() {}

	void navigate(std::string path)
	{
		if(path.empty())
			return;

		if(path == "..")
		{
			path = currentPath.substr(0, currentPath.find_last_of("/\\"));
			if(path.empty())  // when trying to go up ("..") and parent path results in a empty string (probably unix-based OS)
				path = "/";   // assume / as folder
		}

		folderList.clear();
		fileList.clear();

		folderList.push_back("..");

		std::vector<std::string> filenames = fgeal::filesystem::getFilenamesWithinDirectory(path);
		for(unsigned i = 0; i < filenames.size(); i++)
		{
			if(fgeal::filesystem::isFilenameDirectory(filenames[i]))
				folderList.push_back(filenames[i]);
			else
				fileList.push_back(filenames[i]);
		}

		currentPath = path;
		cursor = 0;
	}

	void render()
	{
		fgeal::Display& display = game.getDisplay();
		const fgeal::Rectangle box = {0.1f*display.getWidth(), 0.3f*display.getHeight(), 0.8f*display.getWidth(), 0.6f*display.getHeight()};

		display.clear();

		fgeal::Graphics::drawFilledRectangle(box, fgeal::Color::LIGHT_GREY);

		int offset = 2;

		fgeal::Graphics::drawFilledRectangle(box.x, box.y, box.w, fontSans->getTextHeight(), fgeal::Color::DARK_GREY);
		fontSans->drawText(currentPath, box.x + 2, box.y + offset, fgeal::Color::WHITE);

		offset += fontSans->getTextHeight();
		for(unsigned i = 0; i < folderList.size(); i++)
		{
			if(cursor == i)
				fgeal::Graphics::drawFilledRectangle(box.x, box.y + offset, box.w, fontMonoHeight, fgeal::Color::GREY);
			fontMono->drawText(folderList[i], box.x + 2, box.y + offset, fgeal::Color::NAVY);
			offset += fontMonoHeight;
		}
		for(unsigned i = 0; i < fileList.size(); i++)
		{
			if(cursor == i+folderList.size())
				fgeal::Graphics::drawFilledRectangle(box.x, box.y + offset, box.w, fontMonoHeight, fgeal::Color::GREY);
			fontMono->drawText(fileList[i], box.x + 2, box.y + offset, fgeal::Color::BLACK);
			offset += fontMonoHeight;
		}

		float posTxt = 16;
		fontMono->drawText("File test", 16, posTxt+=fontMonoHeight, fgeal::Color::GREEN);
		fontMono->drawText("Use arrow up and down keys to select the entry in the current folder", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Press enter to enter the current entry, if it is a folder", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Select and press enter key on the \"..\" entry to go to the parent directory", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);

		fontMono->drawText("Press F1 through F9 keys for other tests", 16, game.getDisplay().getHeight() - fontMonoHeight, fgeal::Color::WHITE);
	}

	void update(float delta)
	{}

	void onKeyPressed(fgeal::Keyboard::Key key)
	{
		globalInputHandler(key, game, *this);

		using fgeal::Keyboard;
		switch(key)
		{
			case Keyboard::KEY_ARROW_UP:
				if(cursor == 0)
					cursor = folderList.size()+fileList.size()-1;
				else
					cursor--;
				break;

			case Keyboard::KEY_ARROW_DOWN:
				if(cursor == folderList.size()+fileList.size()-1)
					cursor = 0;
				else
					cursor++;
				break;

			case Keyboard::KEY_ENTER:
				if(cursor < folderList.size())
					navigate(folderList[cursor]);
				break;

			default: break;
		}
	}
};
