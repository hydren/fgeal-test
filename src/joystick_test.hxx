/*
 * joystick_test.hxx
 *
 *  Created on: 8 de nov de 2017
 *      Author: felipe
 */

#include <ciso646>
#include <string>
#include <cmath>
#include <algorithm>
#include <utility>

#include <fgeal/fgeal.hpp>
#include <fgeal/extra/game.hpp>

class JoystickTestState : public fgeal::Game::State
{
	bool pollingMode, offset;

	std::vector< std::pair< std::vector<bool>, std::vector<float> > > joystickEventData;

	public:
	int getId() { return 9; }

	JoystickTestState(fgeal::Game& game)
	: State(game), pollingMode(true), offset(false)
	{}

	~JoystickTestState()
	{}

	void initialize()
	{
		joystickEventData.resize(fgeal::Joystick::getCount(), std::make_pair(std::vector<bool>(), std::vector<float>()));
		for(unsigned j = 0; j < fgeal::Joystick::getCount(); j++)
		{
			joystickEventData[j].first.resize(fgeal::Joystick::getButtonCount(j), false);
			joystickEventData[j].second.resize(fgeal::Joystick::getAxisCount(j), 0);
		}
	}

	void onEnter()
	{
		for(unsigned j = 0; j < fgeal::Joystick::getCount(); j++)
		{
			for(unsigned b = 0; b < fgeal::Joystick::getButtonCount(j); b++)
				joystickEventData[j].first[b] = false;
			for(unsigned a = 0; a < fgeal::Joystick::getAxisCount(j); a++)
				joystickEventData[j].second[a] = 0;
		}
	}

	void onLeave()
	{}

	void render()
	{
		using fgeal::Joystick;
		using fgeal::Color;
		using fgeal::Point;

		fgeal::Display& display = game.getDisplay();
		display.clear();

		if(Joystick::getCount() == 0)
			fontMono->drawText("No joysticks connected", 0.5f*display.getWidth(), 0.5f*display.getHeight(), sin(10*fgeal::uptime())>0? Color::WHITE : Color::CYAN);
		else
		{
			const float headerSize = 10*fontMonoHeight, sw = display.getWidth(), fh = (display.getHeight()-headerSize)/(float)Joystick::getCount();
			for(unsigned j = 0; j < Joystick::getCount(); j++)
			{
				fgeal::Graphics::drawRectangle(0, headerSize+j*fh, sw, headerSize+fh, Color::GREEN);

				const float btnSize = std::min(0.5*sw/ceil(Joystick::getButtonCount(j)/2.0), fh/2.0);
				unsigned b;
				for(b = 0; b < Joystick::getButtonCount(j)/2; b++)
				{
					const bool isButtonPressed = pollingMode? Joystick::isButtonPressed(j, b) : joystickEventData[j].first[b];
					fgeal::Graphics::drawFilledCircle((b+0.5f)*btnSize, headerSize+j*fh + 0.5f*btnSize, 0.5f*btnSize, isButtonPressed? Color::RED : Color::MAROON);
					fgeal::Graphics::drawCircle((b+0.5f)*btnSize, headerSize+j*fh + 0.5f*btnSize, 0.5f*btnSize, Color::RED);
					fontMono->drawText("btn"+toString(b+offset), (b+0.5f)*btnSize, headerSize+j*fh + 0.5f*btnSize, Color::WHITE);
				}
				for(; b < Joystick::getButtonCount(j); b++)
				{
					const bool isButtonPressed = pollingMode? Joystick::isButtonPressed(j, b) : joystickEventData[j].first[b];
					fgeal::Graphics::drawFilledCircle((b+0.5f-Joystick::getButtonCount(j)/2)*btnSize, headerSize+j*fh + 1.5f*btnSize, 0.5f*btnSize, isButtonPressed? Color::RED : Color::MAROON);
					fgeal::Graphics::drawCircle((b+0.5f-Joystick::getButtonCount(j)/2)*btnSize, headerSize+j*fh + 1.5f*btnSize, 0.5f*btnSize, Color::RED);
					fontMono->drawText("btn"+toString(b+offset), (b+0.5f-Joystick::getButtonCount(j)/2)*btnSize, headerSize+j*fh + 1.5f*btnSize, Color::WHITE);
				}

				for(unsigned a = 0; a < Joystick::getAxisCount(j); a++)
				{
					const float axisPos = pollingMode? Joystick::getAxisPosition(j, a) : joystickEventData[j].second[a];
					const float barSize = std::min((0.5f*sw)/Joystick::getAxisCount(j), 0.05f*sw);
					fgeal::Graphics::drawFilledRectangle(0.5f*sw + a*barSize, headerSize+j*fh, barSize, 0.5f*(1.0f+axisPos)*fh, Color::GREEN);
					fgeal::Graphics::drawRectangle(0.5f*sw + a*barSize, headerSize+j*fh, barSize, 0.5f*(1.0f+axisPos)*fh, Color::DARK_GREEN);
					fontMono->drawText("axis"+toString(a+offset), 0.5f*sw + a*barSize, headerSize+j*fh, Color::RED);
					fontMono->drawText(toString(axisPos), 0.5f*sw + a*barSize, headerSize+j*fh+fontMonoHeight, Color::RED);
				}

				std::string joystickName;
				try { joystickName = Joystick::getJoystickName(j); }
				catch (const fgeal::NotImplementedException& e) { joystickName = "--"; }
				fontMono->drawText("Joystick #" + toString(j+offset) + ": " +joystickName, 0, headerSize+j*fh, Color::WHITE);
			}
		}

		float posTxt = 16;
		fontMono->drawText("Joystick test (press F4 again for mouse test)", 16, posTxt+=fontMonoHeight, fgeal::Color::GREEN);
		fontMono->drawText("Pressing joystick buttons lights up respectives circles", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Moving around axis should provoke changes in the visualization bars", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Press the space bar to change between polling mode and event mode", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Press the '1' key to change joystick numbering offset (start at 0 or 1)", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);

		fontMono->drawText(pollingMode? "Polling mode" : "Event mode", display.getWidth() - fontMono->getTextWidth("____________"), 2*fontMonoHeight, pollingMode? Color::YELLOW : Color::CYAN);

		fontMono->drawText("Press F1 through F9 keys for other tests", 16, game.getDisplay().getHeight() - fontMonoHeight, fgeal::Color::WHITE);
	}

	void update(float delta)
	{}

	void onKeyPressed(fgeal::Keyboard::Key key)
	{
		globalInputHandler(key, game, *this);

		if(key == fgeal::Keyboard::KEY_1)
			offset = !offset;

		if(key == fgeal::Keyboard::KEY_SPACE)
			pollingMode = !pollingMode;
	}

	void onJoystickButtonPressed(unsigned joystickIndex, unsigned joystickButtonIndex)
	{
		joystickEventData[joystickIndex].first[joystickButtonIndex] = true;
	}

	void onJoystickButtonReleased(unsigned joystickIndex, unsigned joystickButtonIndex)
	{
		joystickEventData[joystickIndex].first[joystickButtonIndex] = false;
	}

	void onJoystickAxisMoved(unsigned joystickIndex, unsigned joystickAxisIndex, float newValue)
	{
		joystickEventData[joystickIndex].second[joystickAxisIndex] = newValue;
	}
};
