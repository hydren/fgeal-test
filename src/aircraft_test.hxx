/*
 * aircraft_test.hxx
 *
 *  Created on: 13 de fev de 2017
 *      Author: carlosfaruolo
 */

#include <ciso646>
#include <cstdlib>
#include <cmath>

#include <fgeal/fgeal.hpp>
#include <fgeal/exceptions.hpp>
#include <fgeal/extra/game.hpp>

class AircraftTestState : public fgeal::Game::State
{
	fgeal::Image* imgAircraft, *imgCar;
	float posx, posy, angle, vx, vy;
	const float FORCE;
	fgeal::Color bgColor;
	bool carMode;

	public:
	int getId() { return 14; }

	AircraftTestState(fgeal::Game& game)
	: State(game),
	  imgAircraft(null), imgCar(null),
	  posx(0), posy(0), angle(0), vx(0), vy(0),
	  FORCE(25000.0),
	  bgColor(fgeal::Color::GREY),
	  carMode(false)
	{}

	~AircraftTestState()
	{
		delete imgAircraft;
		delete imgCar;
	}

	void initialize()
	{
		imgAircraft = new fgeal::Image("aircraft.png");
		imgCar = new fgeal::Image("car.png");
	}

	void onEnter()
	{
		posx = game.getDisplay().getWidth()/2;
		posy = game.getDisplay().getHeight()/2;
		carMode = false;
		game.setInputManagerEnabled(false);  // needed to handle events manually
	}

	void onLeave()
	{
		game.setInputManagerEnabled();  // needed to get back on not handling events manually
	}

	void render()
	{
		game.getDisplay().clear();
		fgeal::Graphics::drawFilledRectangle(0, 0, game.getDisplay().getWidth(),  game.getDisplay().getHeight(), bgColor);

		fgeal::Image* imgVehicle = (carMode? imgCar : imgAircraft);
		imgVehicle->drawRotated(posx, posy, angle, imgVehicle->getWidth()/2, imgVehicle->getHeight()/2);

		float posTxt = 16;
		fontMono->drawText("Aircraft test game (press F8 for copter test game)", 16, posTxt+=fontMonoHeight, fgeal::Color::GREEN);
		fontMono->drawText("Press left and right arrow keys to rotate the vehicle", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Press up arrow key to throttle the vehicle", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Press space bar to change aircraft to top-down car", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);

		fontMono->drawText("Press F1 through F9 keys for other tests", 16, game.getDisplay().getHeight() - fontMonoHeight, fgeal::Color::WHITE);
	}

	void update(float delta)
	{
		// example of classic event handling of input
		fgeal::Event event;
		fgeal::EventQueue& queue = fgeal::EventQueue::getInstance();
		while(queue.hasEvents())
		{
			queue.getNextEvent(&event);
			if(event.getEventType() == fgeal::Event::TYPE_DISPLAY_CLOSURE)
				game.running = false;

			if(event.getEventType() == fgeal::Event::TYPE_KEY_PRESS)
			{
				globalInputHandler(event.getEventKeyCode(), game, *this);

				if(event.getEventKeyCode() == fgeal::Keyboard::KEY_SPACE)
					carMode = !carMode;
			}
		}

		float speed = sqrt(vx*vx + vy*vy), acc = 0;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_UP))
			acc = FORCE/(speed+1);

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_LEFT))
			angle += delta*10;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_RIGHT))
			angle -= delta*10;

		// acceleration
		vx += sin(angle)*delta*acc;
		vy += cos(angle)*delta*acc;

		// friction
		vx = vx*(1-delta);
		vy = vy*(1-delta);

		// movement
		posx += vx*delta;
		posy += vy*delta;

		// random bg color
		int tmp;
		tmp = 1-rand()%3; bgColor.r = bgColor.r + tmp > 255? 255 : bgColor.r + tmp < 0? 0 : bgColor.r + tmp;
		tmp = 1-rand()%3; bgColor.g = bgColor.g + tmp > 255? 255 : bgColor.g + tmp < 0? 0 : bgColor.g + tmp;
		tmp = 1-rand()%3; bgColor.b = bgColor.b + tmp > 255? 255 : bgColor.b + tmp < 0? 0 : bgColor.b + tmp;
	}
};
