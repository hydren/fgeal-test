/*
 * window_test.hxx
 *
 *  Created on: 14 de mai de 2018
 *      Author: carlosfaruolo
 */

#include <ciso646>

#include <ciso646>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>

#include <fgeal/fgeal.hpp>
#include <fgeal/extra/game.hpp>

class WindowTest : public fgeal::Game::State
{
	fgeal::Point line1[2], line2[2], tri[3], quad[4], circleCenter, ellipseCenter, radii;
	fgeal::Rectangle rectangle;
	float radius;
	fgeal::Image* img;

	bool mouseCursorVisible;

	std::vector<fgeal::Display::Mode> modes, genericModes;

	public:
	int getId() { return 11; }

	WindowTest(fgeal::Game& game)
	: State(game), radius(0), img(null), mouseCursorVisible(true)
	{}

	~WindowTest()
	{
		delete img;
	}

	void initialize()
	{
		srand(time(null));
		img = new fgeal::Image("icon.png");
	}

	void recalculateGeometry()
	{
		fgeal::Display& display = game.getDisplay();
		const float w = display.getWidth(), h = display.getHeight();

		line1[0].x = 0.76*w; line1[0].y = 0.71*h;
		line1[1].x = 0.99*w; line1[1].y = 0.71*h;

		line2[0].x = 0.76*w; line2[0].y = 0.72*h;
		line2[1].x = 0.99*w; line2[1].y = 0.74*h;

		tri[0].x = 0.76*w;  tri[0].y = 0.32*h;
		tri[1].x = 0.86*w;  tri[1].y = 0.32*h;
		tri[2].x = 0.81*w;  tri[2].y = 0.27*h;

		rectangle.x = 0.88*w; rectangle.y = 0.27*h;
		rectangle.w = 0.10*w; rectangle.h = 0.05*h;

		quad[0].x = 0.76*w; quad[0].y = 0.35*h;
		quad[1].x = 0.85*w; quad[1].y = 0.35*h;
		quad[2].x = 0.90*w; quad[2].y = 0.40*h;
		quad[3].x = 0.85*w; quad[3].y = 0.40*h;

		circleCenter.x = 0.95*w; circleCenter.y = 0.37*h;
		radius = 0.00004*w*h;

		ellipseCenter.x = 0.88*w; ellipseCenter.y = 0.5*h;
		radii.x = 0.075*w;
		radii.y = 0.075*h;
	}

	void onEnter()
	{
		this->recalculateGeometry();
		genericModes = fgeal::Display::Mode::getGenericList();
		modes = fgeal::Display::Mode::getList();
	}

	void onLeave() {}

	void render()
	{
		fgeal::Display& display = game.getDisplay();
		display.clear();

		fgeal::Graphics::drawFilledRectangle(0.75*display.getWidth(), 0.25*display.getHeight(), 0.25*display.getWidth(), 0.5*display.getHeight());

		fgeal::Graphics::drawLine(line1[0], line1[1], fgeal::Color::GREEN);
		fgeal::Graphics::drawLine(line2[0], line2[1], fgeal::Color::GREEN);

		fgeal::Graphics::drawFilledTriangle(tri[0], tri[1], tri[2], fgeal::Color::RED);
		fgeal::Graphics::drawFilledRectangle(rectangle, fgeal::Color::BLUE);
		fgeal::Graphics::drawFilledQuadrangle(quad[0], quad[1], quad[2], quad[3], fgeal::Color::YELLOW);
		fgeal::Graphics::drawFilledCircle(circleCenter, radius, fgeal::Color::MAGENTA);
		fgeal::Graphics::drawFilledEllipse(ellipseCenter, radii, fgeal::Color::CYAN);

		img->drawScaled(0.835*display.getWidth(), 0.6*display.getHeight(), 128.f/display.getHeight(), 128.f/display.getHeight());

		fontMono->drawText("Sample images: ", 0.75f*display.getWidth(), 0.25f*display.getHeight(), fgeal::Color::TEAL);

		fontMono->drawText("Fullscreen modes: ", 0.05f*display.getWidth(), 0.25f*display.getHeight(), fgeal::Color::MAGENTA);

		for(unsigned i = 0; i < modes.size(); i++)
		{
			fgeal::Display::Mode& mode = modes[i];
			string modeTxt;
			modeTxt += toString(mode.width);
			modeTxt += "x";
			modeTxt += toString(mode.height);
			modeTxt += " ";
			modeTxt += toString(mode.description);
			fontMono->drawText(modeTxt, 0.05f*display.getWidth(), 0.25f*display.getHeight() + (1+i)*fontMonoHeight, fgeal::Color::RED);
		}

		fontMono->drawText("Common windowed modes: ", 0.3f*display.getWidth(), 0.25f*display.getHeight(), fgeal::Color::CYAN);

		for(unsigned i = 0; i < genericModes.size(); i++)
		{
			fgeal::Display::Mode& mode = genericModes[i];
			string modeTxt;
			modeTxt += toString(mode.width);
			modeTxt += "x";
			modeTxt += toString(mode.height);
			modeTxt += " ";
			modeTxt += toString(mode.description);
			fontMono->drawText(modeTxt, 0.3f*display.getWidth(), 0.25f*display.getHeight() + (1+i)*fontMonoHeight, fgeal::Color::BLUE);
		}

		fontMono->drawText("Current window size: "+toString(display.getWidth())+"x"+toString(display.getHeight()), 0.5f*display.getWidth(), 0.25f*display.getHeight(), sin(10*fgeal::uptime())>0? fgeal::Color::ORANGE : fgeal::Color::MAROON);

		float posTxt = 16;
		fontMono->drawText("Window test", 16, posTxt+=fontMonoHeight, fgeal::Color::GREEN);
		fontMono->drawText("Press the E-R keys to change window size forward and backward", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Press the P key to change window position to a random one", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Press the C key to center the window", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Press the F key to toggle fullscreen mode", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Press the M key to toggle visibility of mouse cursor", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);

		fontMono->drawText("Press F1 through F9 keys for other tests", 16, game.getDisplay().getHeight() - fontMonoHeight, fgeal::Color::WHITE);
	}

	void update(float delta)
	{}

	void onKeyPressed(fgeal::Keyboard::Key key)
	{
		globalInputHandler(key, game, *this);

		if(key == fgeal::Keyboard::KEY_R or key == fgeal::Keyboard::KEY_E)
		{
			std::vector<fgeal::Display::Mode> modes = game.getDisplay().isFullscreen()? fgeal::Display::Mode::getList() : fgeal::Display::Mode::getGenericList();
			for(unsigned i = 0; i < modes.size(); i++)
			if(modes[i].width == game.getDisplay().getWidth() and modes[i].height == game.getDisplay().getHeight())
			{
				const bool fwd = (key == fgeal::Keyboard::KEY_R);
				fgeal::Display::Mode mode = modes[(fwd?(i+1):(i-1))%modes.size()];
				std::cout << "setting mode " << mode.width << "x" << mode.height << std::endl;
				game.getDisplay().setSize(mode.width, mode.height);
				break;
			}
			this->recalculateGeometry();
		}

		if(key == fgeal::Keyboard::KEY_P)
		{
			const fgeal::Point randomPos = { (float) (rand() % 64), (float) (rand() % 64) };
			game.getDisplay().setPosition(randomPos);
		}

		if(key == fgeal::Keyboard::KEY_C)
			game.getDisplay().setPositionOnCenter();

		if(key == fgeal::Keyboard::KEY_F)
			game.getDisplay().setFullscreen(!game.getDisplay().isFullscreen());

		if(key == fgeal::Keyboard::KEY_M)
			game.getDisplay().setMouseCursorVisible(mouseCursorVisible=!mouseCursorVisible);
	}
};
