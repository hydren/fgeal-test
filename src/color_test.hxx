/*
 * color_test.hxx
 *
 *  Created on: 20 de jun de 2017
 *      Author: carlosfaruolo
 */

#include <ciso646>

#include <fgeal/fgeal.hpp>
#include <fgeal/extra/game.hpp>

class ColorTestState : public fgeal::Game::State
{
	fgeal::Color color1;
	unsigned char cursor;

	public:
	int getId() { return 5; }

	ColorTestState(fgeal::Game& game)
	: State(game), color1(fgeal::Color::create(128, 128, 128)), cursor(0)
	{}

	~ColorTestState()
	{}

	void initialize()
	{}

	void onEnter()
	{}

	void onLeave()
	{}

	void render()
	{
		fgeal::Display& display = game.getDisplay();
		const unsigned w = display.getWidth(), h = display.getHeight();

		display.clear();
		fgeal::Graphics::drawFilledRectangle(w*0.25, h*0.25, w*0.5, h*0.25, color1);
		fgeal::Graphics::drawFilledCircle(w*(0.25 + 0.25*cursor), h*0.75, w*0.130, fgeal::Color::WHITE);
		fgeal::Graphics::drawFilledCircle(w*0.25,  h*0.75, w*0.125, fgeal::Color::RED);
		fgeal::Graphics::drawFilledCircle(w*0.5,   h*0.75, w*0.125, fgeal::Color::GREEN);
		fgeal::Graphics::drawFilledCircle(w*0.75,  h*0.75, w*0.125, fgeal::Color::BLUE);
		fgeal::Graphics::drawFilledTriangle(w*(0.225 + 0.25*cursor), h*0.625, w*(0.25 + 0.25*cursor), h*0.6, w*(0.275 + 0.25*cursor), h*0.625, fgeal::Color::BLACK);
		fgeal::Graphics::drawFilledTriangle(w*(0.225 + 0.25*cursor), h*0.85, w*(0.25 + 0.25*cursor), h*0.875, w*(0.275 + 0.25*cursor), h*0.85, fgeal::Color::BLACK);

		fontMono->drawText(toString((int) color1.r), w*0.25,  h*0.75, fgeal::Color::BLACK);
		fontMono->drawText(toString((int) color1.g), w*0.5 ,  h*0.75, fgeal::Color::BLACK);
		fontMono->drawText(toString((int) color1.b), w*0.75,  h*0.75, fgeal::Color::BLACK);
		fontMono->drawText(toString((int) color1.a), w*0.9,  h*0.75, fgeal::Color::RED);

		fontMono->drawText("test", w*0.25, h*0.25 - fontMonoHeight, color1);

		float posTxt = 16;
		fontMono->drawText("Color test (press F2 again for primitives test)", 16, posTxt+=fontMonoHeight, fgeal::Color::GREEN);
		fontMono->drawText("Left and right arrow keys controls which color component to change", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Up and down arrow keys increase or decrease amount of selected color component", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("The color components are, in order, red, green, blue and alpha", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);

		fontMono->drawText("Press F1 through F9 keys for other tests", 16, game.getDisplay().getHeight() - fontMonoHeight, fgeal::Color::WHITE);
	}

	void update(float delta)
	{}

	void onKeyPressed(fgeal::Keyboard::Key key)
	{
		globalInputHandler(key, game, *this);

		if(key == fgeal::Keyboard::KEY_ARROW_LEFT)
			if(cursor > 0) cursor--;

		if(key == fgeal::Keyboard::KEY_ARROW_RIGHT)
			if(cursor < 3) cursor++;

		if(key == fgeal::Keyboard::KEY_ARROW_UP or key == fgeal::Keyboard::KEY_ARROW_DOWN)
		{
			unsigned char* component = null;
			switch(cursor)
			{
				case 0: component = &color1.r; break;
				case 1: component = &color1.g; break;
				case 2: component = &color1.b; break;
				case 3: component = &color1.a; break;
				default: break;
			}
			bool decrease = (key == fgeal::Keyboard::KEY_ARROW_DOWN);
			int value = (decrease? -1 : 1) * (*component == 255 and decrease? 7 : 8) + (int) *component;
			if(value > 255) value = 255;
			if(value < 0)   value = 0;
			*component = value;
		}
	}
};
