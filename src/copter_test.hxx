/*
 * copter_test.hxx
 *
 *  Created on: 13 de fev de 2017
 *      Author: carlosfaruolo
 */

#include <ciso646>
#include <iostream>
#include <cstdlib>
#include <cmath>

#include <fgeal/fgeal.hpp>
#include <fgeal/extra/game.hpp>

class CopterTestState : public fgeal::Game::State
{
	fgeal::Image* imgCopter, *imgLandscape;
	float posx, posy;
	bool reflected;
	const float FORCE;

	public:
	int getId() { return 13; }

	CopterTestState(fgeal::Game& game)
	: State(game),
	  imgCopter(null), imgLandscape(null),
	  posx(0), posy(0), reflected(false),
	  FORCE(250.0)
	{}

	~CopterTestState()
	{
		delete imgCopter;
		delete imgLandscape;
	}

	void initialize()
	{
		imgLandscape = new fgeal::Image("landscape.jpg");
		imgCopter = new fgeal::Image("heli.png");
	}

	void onEnter()
	{
		posx = 0;
		posy = 0;
		reflected = false;
	}

	void onLeave() {}

	void render()
	{
		game.getDisplay().clear();
		imgLandscape->draw(-posx, -posy + 5*sin(5*fgeal::uptime()));
		imgCopter->drawFlipped(game.getDisplay().getWidth()/2, game.getDisplay().getHeight()/2, reflected? fgeal::Image::FLIP_HORIZONTAL : fgeal::Image::FLIP_NONE);

		float posTxt = 16;
		fontMono->drawText("Copter test game (press F8 for aircraft test game)", 16, posTxt+=fontMonoHeight, fgeal::Color::GREEN);
		fontMono->drawText("Use arrow keys to pilot the chopper", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);

		fontMono->drawText("Press F1 through F9 keys for other tests", 16, game.getDisplay().getHeight() - fontMonoHeight, fgeal::Color::WHITE);
	}

	void update(float delta)
	{
		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_UP))
			posy -= FORCE*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_DOWN))
			posy += FORCE*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_LEFT))
		{
			posx -= FORCE*delta;
			reflected = false;
		}

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_RIGHT))
		{
			posx += FORCE*delta;
			reflected = true;
		}
	}

	void onKeyPressed(fgeal::Keyboard::Key key)
	{
		globalInputHandler(key, game, *this);
	}
};
