/*
 * sprite_test.hxx
 *
 *  Created on: 13 de fev de 2017
 *      Author: carlosfaruolo
 */

#include <ciso646>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <map>

#include <fgeal/fgeal.hpp>
#include <fgeal/extra/game.hpp>
#include <fgeal/extra/sprite.hpp>

class SpriteTestState : public fgeal::Game::State
{
	fgeal::Image* imgCharSheet;
	std::map<std::string, fgeal::Sprite*> sprites;
	std::string currentAnim;

	float posx, posy, angle;
	fgeal::Vector2D scale;
	fgeal::Rectangle croppingArea;
	const float speed;
	bool whiteBg;

	public:
	int getId() { return 2; }

	SpriteTestState(fgeal::Game& game)
	: State(game),
	  imgCharSheet(null),
	  posx(0), posy(0), angle(0), scale(),
	  speed(150.0), whiteBg()
	{}

	~SpriteTestState()
	{
		delete imgCharSheet;
		for(std::map<std::string, fgeal::Sprite*>::iterator it = sprites.begin(); it != sprites.end(); ++it)
			delete it->second;
	}

	void initialize()
	{
		imgCharSheet = new fgeal::Image("sprite.png");
		sprites["still-right"] = new fgeal::Sprite(*imgCharSheet, 46, 50, 0.5, 1, 0, 0);
		sprites["still-left"] = new fgeal::Sprite(*imgCharSheet, 46, 50, 0.5, 1, 0, 0);
		sprites["still-left"]->flipmode = fgeal::Image::FLIP_HORIZONTAL;
		sprites["walking-right"] = new fgeal::Sprite(*imgCharSheet, 46, 50, 0.06, 8, 0, 50);
		sprites["walking-left"] = new fgeal::Sprite(*imgCharSheet, 46, 50, 0.06, 8, 0, 50);
		sprites["walking-left"]->flipmode = fgeal::Image::FLIP_HORIZONTAL;
		currentAnim = "still-right";
	}

	void onEnter()
	{
		posx = game.getDisplay().getWidth()/2;
		posy = game.getDisplay().getHeight()/2;
		angle = 0;
		scale.x = scale.y = 1.0;
		croppingArea.x = croppingArea.y = 0;
		croppingArea.w = sprites.begin()->second->width;
		croppingArea.h = sprites.begin()->second->height;
		whiteBg = true;
	}

	void onLeave() {}

	void render()
	{
		game.getDisplay().clear();
		if(whiteBg) fgeal::Graphics::drawFilledRectangle(0, 0, game.getDisplay().getWidth(), game.getDisplay().getHeight());

		sprites[currentAnim]->computeCurrentFrame();
		sprites[currentAnim]->angle = (currentAnim == "walking-left" or currentAnim == "still-left"? -angle : angle);
		sprites[currentAnim]->scale = scale;
		sprites[currentAnim]->croppingArea = croppingArea;
		sprites[currentAnim]->draw(posx, posy);
		fontMono->drawText(std::string("x scale ") + toString(scale.x), 16, 128, fgeal::Color::ORANGE);
		fontMono->drawText(std::string("y scale ") + toString(scale.y), 16, 144, fgeal::Color::ORANGE);
		fontMono->drawText(std::string("angle ") + toString(angle), 16, 176, fgeal::Color::VIOLET);
		fontMono->drawText(std::string("cropping area x ") + toString(croppingArea.x), 16, 208, fgeal::Color::TEAL);
		fontMono->drawText(std::string("cropping area y ") + toString(croppingArea.y), 16, 224, fgeal::Color::TEAL);
		fontMono->drawText(std::string("cropping area w ") + toString(croppingArea.w), 16, 240, fgeal::Color::TEAL);
		fontMono->drawText(std::string("cropping area h ") + toString(croppingArea.h), 16, 256, fgeal::Color::TEAL);

		const fgeal::Color textColor = whiteBg? fgeal::Color::BLACK : fgeal::Color::WHITE;

		float posTxt = 16;
		fontMono->drawText("Sprite-class drawing test (press F1 again for blit test)", 16, posTxt+=fontMonoHeight, fgeal::Color::DARK_GREEN);
		fontMono->drawText("Arrow keys controls position; when moving left, the sprite is drawn flipped", 16, posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("A-Q keys controls y-scale, Z-X keys controls x-scale, W-E keys controls both); press S to reset scale to 1.0", 16, posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press R-T keys to changle angle of rotation; press Y to reset angle to zero", 16, posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press space to switch the background from white/black", 16, posTxt+=fontMonoHeight, textColor);

		fontMono->drawText("Press F1 through F9 keys for other tests", 16, game.getDisplay().getHeight() - fontMonoHeight, textColor);
	}

	void update(float delta)
	{
		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_A))
			scale.y = scale.y/(1+delta);
		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_Q))
			scale.y = scale.y*(2-1/(1+delta));

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_Z))
			scale.x = scale.x/(1+delta);
		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_X))
			scale.x = scale.x*(2-1/(1+delta));

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_W))
		{	scale.x = scale.x/(1+delta); scale.y = scale.y/(1+delta); }
		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_E))
		{	scale.x = scale.x*(2-1/(1+delta)); scale.y = scale.y*(2-1/(1+delta)); }

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_S))
			scale.x = scale.y = 1.0f;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_R))
			angle += 0.5*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_T))
			angle -= 0.5*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_Y))
			angle = 0;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_UP))
			posy -= speed*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_DOWN))
			posy += speed*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_LEFT))
			posx -= speed*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_RIGHT))
			posx += speed*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_NUMPAD_8))
			croppingArea.x -= speed*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_NUMPAD_2))
			croppingArea.x += speed*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_NUMPAD_4))
			croppingArea.y -= speed*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_NUMPAD_6))
			croppingArea.y += speed*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_NUMPAD_7))
			croppingArea.h -= speed*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_NUMPAD_1))
			croppingArea.h += speed*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_NUMPAD_9))
			croppingArea.w -= speed*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_NUMPAD_3))
			croppingArea.w += speed*delta;
	}

	void onKeyPressed(fgeal::Keyboard::Key key)
	{
		globalInputHandler(key, game, *this);

		if(key == fgeal::Keyboard::KEY_ARROW_RIGHT)
			currentAnim = "walking-right";

		if(key == fgeal::Keyboard::KEY_ARROW_LEFT)
			currentAnim = "walking-left";

		if(key == fgeal::Keyboard::KEY_SPACE)
			whiteBg = !whiteBg;
	}

	void onKeyReleased(fgeal::Keyboard::Key key)
	{
		if(key == fgeal::Keyboard::KEY_ARROW_RIGHT)
			currentAnim = "still-right";

		if(key == fgeal::Keyboard::KEY_ARROW_LEFT)
			currentAnim = "still-left";
	}
};
