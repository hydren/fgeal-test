/*
 * gui_test.hxx
 *
 *  Created on: 6 de abr de 2023
 *      Author: carlosfaruolo
 */

#include <fgeal/fgeal.hpp>
#include <fgeal/extra/game.hpp>
#include <fgeal/extra/gui.hpp>

class GUITestState : public fgeal::Game::State
{
	fgeal::Font *fontSans, *fontSerif;

	fgeal::Panel panel, dummypanel1, dummypanel2;
	fgeal::Label title, dummylabel;
	fgeal::TextField textfield;
	fgeal::Button buttonTestContent, buttonTestAlignment, buttonTestOverflow;
	fgeal::IconButton iconbutton;
	fgeal::ArrowIconButton arrowbutton;
	fgeal::TabbedPane tabbedpane;
	fgeal::Menu menu;

	public:
	int getId() { return 15; }

	GUITestState(fgeal::Game& game)
	: State(game), fontSans(), fontSerif(), panel(), title(), textfield(),
	  buttonTestContent(), buttonTestAlignment(), buttonTestOverflow(), iconbutton()
	{}

	~GUITestState()
	{
		delete fontSans;
		delete fontSerif;
		delete iconbutton.icon;
	}

	void initialize()
	{
		fontSans = new fgeal::Font("freesans.ttf");
		fontSerif = new fgeal::Font("freeserif.ttf", 20);

		using fgeal::Color;
		panel.backgroundColor = Color::CREAM;
		panel.borderColor = Color::CARAMEL;
		panel.borderDisabled = false;

		title.text = "Title as label";
		title.text.setColor(Color::RED);
		title.text.setFont(fontSerif);
		title.backgroundColor = Color::LIGHT_GREY.getTransparent();
		title.padding.x = 2;
		panel.addComponent(title);

		textfield.content = "Type here";
		textfield.text.setColor(Color::GREEN);
		textfield.text.setFont(fontSans);
		textfield.backgroundColor = Color::DARK_GREY.getTransparent();
		panel.addComponent(textfield);

		buttonTestContent.text = "Replace content";
		buttonTestContent.text.setFont(fontSerif);
		buttonTestContent.text.setColor(Color::BLUE);
		buttonTestContent.backgroundColor = Color::NAVY;
		buttonTestContent.borderColor = Color::TEAL;
		buttonTestContent.shape = fgeal::Button::SHAPE_ROUNDED_RECTANGULAR;
		panel.addComponent(buttonTestContent);

		buttonTestAlignment = buttonTestContent;
		buttonTestAlignment.text = "Switch alignment";
		buttonTestAlignment.text.setColor(Color::RED);
		buttonTestAlignment.backgroundColor = Color::MAROON;
		panel.addComponent(buttonTestAlignment);

		buttonTestOverflow = buttonTestContent;
		buttonTestOverflow.text = "Switch overflow";
		buttonTestOverflow.text.setColor(Color::GREEN);
		buttonTestOverflow.backgroundColor = Color::DARK_GREEN;
		panel.addComponent(buttonTestOverflow);

		iconbutton.backgroundColor = Color::MAROON;
		iconbutton.borderColor = Color::BROWN;
		iconbutton.icon = new fgeal::Image("icon_x.png");
		panel.addComponent(iconbutton);

		arrowbutton.backgroundColor = Color::TEAL;
		arrowbutton.arrowOrientation = fgeal::ArrowIconButton::ARROW_RIGHT;
		arrowbutton.borderColor = arrowbutton.arrowColor = Color::CYAN;
		panel.addComponent(arrowbutton);

		tabbedpane.backgroundColor = Color::SALMON;
		tabbedpane.borderColor = Color::SCARLET;
		tabbedpane.text.setFont(fontSans);
		panel.addComponent(tabbedpane);

		dummypanel1.borderColor = Color::LIGHT_GREY;
		dummypanel1.borderDisabled = false;

		dummylabel.text = "Lorem ipsum dolor sit amet...";
		dummylabel.text.setFont(fontSerif);
		dummylabel.text.setColor(~Color::JADE);
		dummylabel.backgroundColor = Color::create(127, 127, 127, 127);
		dummypanel1.addComponent(dummylabel);
		tabbedpane.addTab(dummypanel1, "tab 1");

		menu.setFont(fontSans);
		menu.setColor(Color::DARK_GREEN);
		menu.title.setContent("Simple menu");
		menu.addEntry("First entry");
		menu.addEntry("Another entry");
		menu.addEntry("Changing entry");
		menu.addEntry("Last entry");
		dummypanel2.addComponent(menu);
		tabbedpane.addTab(dummypanel2, "tab 2");
	}

	void onEnter()
	{
		const float dw = fgeal::Display::getInstance().getWidth(), dh = fgeal::Display::getInstance().getHeight();
		panel.bounds.x = 0.1 * dw;
		panel.bounds.y = 0.1 * dh;
		panel.bounds.w = 0.8 * dw;
		panel.bounds.h = 0.8 * dh;
		panel.borderThickness = 2;

		title.bounds.x = panel.bounds.x + 2;
		title.bounds.y = panel.bounds.y + 2;
		title.pack();

		textfield.bounds.x = panel.bounds.x + 2;
		textfield.bounds.y = title.bounds.y + title.bounds.h + 2;
		textfield.packToWidth(0.25 * dw);

		buttonTestContent.padding.x = buttonTestContent.borderThickness = panel.borderThickness;
		buttonTestContent.pack();
		buttonTestContent.bounds.x = panel.bounds.x + (panel.bounds.w - buttonTestContent.bounds.w)/2;
		buttonTestContent.bounds.y = panel.bounds.y + panel.bounds.h - buttonTestContent.bounds.h - 4;

		buttonTestAlignment.padding.x = buttonTestAlignment.borderThickness = buttonTestContent.borderThickness;
		buttonTestAlignment.pack();
		buttonTestAlignment.bounds.x = buttonTestContent.bounds.x - 1.1 * buttonTestAlignment.bounds.w;
		buttonTestAlignment.bounds.y = buttonTestContent.bounds.y;

		buttonTestOverflow.padding.x = buttonTestOverflow.borderThickness = buttonTestContent.borderThickness;
		buttonTestOverflow.pack();
		buttonTestOverflow.bounds.x = buttonTestContent.bounds.x + 1.1 * buttonTestOverflow.bounds.w;
		buttonTestOverflow.bounds.y = buttonTestContent.bounds.y;

		iconbutton.borderThickness = panel.borderThickness;
		iconbutton.bounds.w = iconbutton.bounds.h = 0.03 * dh;
		iconbutton.bounds.x = panel.bounds.x + panel.bounds.w - iconbutton.bounds.w - 1;
		iconbutton.bounds.y = panel.bounds.y + 1;

		arrowbutton.borderThickness = panel.borderThickness;
		arrowbutton.bounds.w = arrowbutton.bounds.h = 0.05 * dh;
		arrowbutton.bounds.x = panel.bounds.x + 4;
		arrowbutton.bounds.y = panel.bounds.y + panel.bounds.h - arrowbutton.bounds.h - 4;

		tabbedpane.bounds.x = dw/4;
		tabbedpane.bounds.y = dh/4;
		tabbedpane.bounds.w = dw/2;
		tabbedpane.bounds.h = dh/2;
		tabbedpane.borderThickness = panel.borderThickness;
		tabbedpane.pack();

		dummypanel1.bounds.w = 0.8 * tabbedpane.bounds.w;
		dummypanel1.bounds.h = 0.8 * tabbedpane.bounds.h;
		dummypanel1.bounds.x = tabbedpane.bounds.x + (tabbedpane.bounds.w - dummypanel1.bounds.w)/2;
		dummypanel1.bounds.y = tabbedpane.bounds.y + (tabbedpane.bounds.h - dummypanel1.bounds.h)/2;

		dummylabel.bounds.x = dummypanel1.bounds.x + 2;
		dummylabel.bounds.y = dummypanel1.bounds.y + 2;
		dummylabel.pack();

		menu.bounds = dummypanel2.bounds = dummypanel1.bounds;
		menu.updateDrawableText();
	}

	void onLeave() {}

	void render()
	{
		game.getDisplay().clear();
		panel.draw();

		float posTxt = 16;
		fontMono->drawText("GUI test", 16, posTxt+=fontMonoHeight, fgeal::Color::GREEN);
		fontMono->drawText("Type characters to edit the content of the text field; pressing backspace delete character at caret position.", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);
		fontMono->drawText("Buttons are clickable, with some having actions", 16, posTxt+=fontMonoHeight, fgeal::Color::WHITE);

		fontMono->drawText("Press F1 through F9 keys for other tests", 16, game.getDisplay().getHeight() - fontMonoHeight, fgeal::Color::WHITE);
	}

	void update(float delta)
	{}

	void onKeyPressed(fgeal::Keyboard::Key key)
	{
		globalInputHandler(key, game, *this);
		bool erase = false;
		using fgeal::Keyboard;
		switch(key)
		{
				// caret
			case Keyboard::KEY_ARROW_LEFT:
				if(textfield.caretPosition > 0) textfield.caretPosition--;
				break;

			case Keyboard::KEY_ARROW_RIGHT:
				if(textfield.caretPosition < textfield.content.length()) textfield.caretPosition++;
				break;

			case Keyboard::KEY_BACKSPACE:
				erase = true;
				break;

			case Keyboard::KEY_NUMPAD_ADDITION:
				fontSans->setSize(fontSans->getSize()+2);
				fontSerif->setSize(fontSerif->getSize()+2);
				break;

			case Keyboard::KEY_NUMPAD_SUBTRACTION:
				fontSans->setSize(fontSans->getSize()-2);
				fontSerif->setSize(fontSerif->getSize()-2);
				break;

			default: break;
		}

		const char typed = fgeal::Keyboard::parseKeyStroke(key);
		if(typed != '\n')
		{
			textfield.content.insert(textfield.content.begin() + textfield.caretPosition, 1, typed);
			textfield.updateDrawableText();
			textfield.caretPosition++;
		}

		if(erase and not textfield.content.empty() and textfield.caretPosition > 0)
		{
			textfield.content.erase(textfield.content.begin() + textfield.caretPosition-1);
			textfield.updateDrawableText();
			textfield.caretPosition--;
		}
	}

	void onMouseButtonPressed(fgeal::Mouse::Button btn, int x, int y)
	{
		if(btn == fgeal::Mouse::BUTTON_LEFT)
		{
			if(tabbedpane.isActiveTab(dummypanel2) and menu.bounds.contains(x, y))
				menu.setSelectedIndexByLocation(x, y);

			else if(tabbedpane.bounds.contains(x, y))
				tabbedpane.setActiveTabByButtonPosition(x, y);

			else if(buttonTestContent.bounds.contains(x, y))
			{
				dummylabel.text = textfield.content;
				menu.getEntryAt(2).label = textfield.content;
				menu.updateDrawableText();
			}

			else if(buttonTestAlignment.bounds.contains(x, y))
				menu.textHorizontalAlignment = dummylabel.textHorizontalAlignment = static_cast<fgeal::Label::Alignment>((dummylabel.textHorizontalAlignment + 1) % 3);

			else if(buttonTestOverflow.bounds.contains(x, y))
			{
				menu.entryOverflowBehavior = textfield.overflowBehavior = static_cast<fgeal::TextField::OverflowBehavior>((textfield.overflowBehavior + 1) % 5);
				textfield.updateDrawableText();
				menu.updateDrawableText();
			}
		}
	}
};
