/*
 * arrow_test.hxx
 *
 *  Created on: 13 de fev de 2017
 *      Author: carlosfaruolo
 */

#include <ciso646>
#include <iostream>
#include <cstdlib>
#include <cmath>

#include <fgeal/fgeal.hpp>
#include <fgeal/extra/game.hpp>
#include <fgeal/exceptions.hpp>

class ArrowTestState : public fgeal::Game::State
{
	fgeal::Image* imgArrow;
	float posx, posy, xscale, yscale, angle, rotx, roty;
	bool noRotation, noScale, printDeltas;
	fgeal::Image::FlipMode flip;
	const float FORCE;
	bool whiteBg;

	public:
	int getId() { return 1; }

	ArrowTestState(fgeal::Game& game)
	: State(game),
	  imgArrow(null),
	  posx(0), posy(0), xscale(1), yscale(1), angle(0), rotx(0), roty(0),
	  noRotation(true), noScale(true), printDeltas(false),
	  flip(fgeal::Image::FLIP_NONE),
	  FORCE(300.0), whiteBg()
	{}

	~ArrowTestState()
	{
		delete imgArrow;
	}

	void initialize()
	{
		imgArrow = new fgeal::Image("arrow.png");
	}

	void onEnter()
	{
		posx = game.getDisplay().getWidth()/2;
		posy = game.getDisplay().getHeight()/2;
		angle = 0;
		rotx = imgArrow->getWidth()/2;
		roty = imgArrow->getHeight()/2;
		xscale = yscale = 1;
		noRotation = noScale = true;
		flip = fgeal::Image::FLIP_NONE;
		whiteBg = false;
	}

	void onLeave() {}

	void render()
	{
		game.getDisplay().clear();
		if(whiteBg) fgeal::Graphics::drawFilledRectangle(0, 0, game.getDisplay().getWidth(), game.getDisplay().getHeight());

		const fgeal::Color textColor = whiteBg? fgeal::Color::BLACK : fgeal::Color::WHITE;

		if(noRotation and noScale and not flip)
		{
			imgArrow->draw(posx, posy);
			fontMono->drawText("using Image::draw()", 16, 0.5*game.getDisplay().getHeight(), textColor);
		}
		else if(noRotation and noScale and flip)
		{
			imgArrow->drawFlipped(posx, posy, flip);
			fontMono->drawText("using Image::drawFlipped()", 16, 0.5*game.getDisplay().getHeight(), textColor);
		}
		else if(noRotation)
		{
			imgArrow->drawScaled(posx, posy, xscale, yscale, flip);
			fontMono->drawText("using Image::drawScaled()", 16, 0.5*game.getDisplay().getHeight(), textColor);
		}
		else if(noScale)
		{
			imgArrow->drawRotated(posx, posy, angle, rotx, roty, flip);
			fontMono->drawText("using Image::drawRotated()", 16, 0.5*game.getDisplay().getHeight(), textColor);
		}
		else
		{
			imgArrow->drawScaledRotated(posx, posy, xscale, yscale, angle, rotx, roty, flip);
			fontMono->drawText("using Image::drawScaledRotated()", 16, 0.5*game.getDisplay().getHeight(), textColor);
		}

		if(not noScale) fontMono->drawText(std::string("scale ") + toString(xscale) + " | " + toString(yscale), 16, 160, textColor);
		if(not noRotation)
		{
			fontMono->drawText(std::string("angle ") + toString(angle), 16, 160+fontMonoHeight, textColor);
			fontMono->drawText(std::string("center: sx=") + toString(rotx) + std::string(" sy=") + toString(roty), 16, 160+2*fontMonoHeight, textColor);
		}

		float posTxt = 16;
		fontMono->drawText("Image drawing test (press F1 again for sprite test)", 16, posTxt+=fontMonoHeight, fgeal::Color::GREEN);
		fontMono->drawText("Arrow keys controls position; press F to flip the image", 16, posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Keypad arrow keys controls center of rotation position",16, posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press S for scaled drawing (A-Q controls y-scale, Z-X controls x-scale, W-E controls both); press R for rotated drawing", 16, posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press D to print game cycle deltas in the standard output", 16, posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press space to switch the background from white/black", 16, posTxt+=fontMonoHeight, textColor);
		fontMono->drawText("Press control to toogle image smoothing", 16, posTxt+=fontMonoHeight, textColor);

		fontMono->drawText("Press F1 through F9 keys for other tests", 16, game.getDisplay().getHeight() - fontMonoHeight, textColor);
	}

	void update(float delta)
	{
		if(noRotation)
		{
			if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_UP))
				posy -= FORCE*delta;

			if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_DOWN))
				posy += FORCE*delta;

			if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_LEFT))
				posx -= FORCE*delta;

			if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_RIGHT))
				posx += FORCE*delta;
		}
		else
		{
			if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_UP))
			{
				posy -= cos(angle)*FORCE*delta;
				posx -= sin(angle)*FORCE*delta;
			}

			if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_LEFT))
				angle += delta*8;

			if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_ARROW_RIGHT))
				angle -= delta*8;
		}

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_NUMPAD_8))
			roty -= FORCE*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_NUMPAD_2))
			roty += FORCE*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_NUMPAD_4))
			rotx -= FORCE*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_NUMPAD_6))
			rotx += FORCE*delta;

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_A))
			yscale = yscale/(1+delta);
		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_Q))
			yscale = yscale*(2-1/(1+delta));

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_Z))
			xscale = xscale/(1+delta);
		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_X))
			xscale = xscale*(2-1/(1+delta));

		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_W))
		{	xscale = xscale/(1+delta); yscale = yscale/(1+delta); }
		if(fgeal::Keyboard::isKeyPressed(fgeal::Keyboard::KEY_E))
		{	xscale = xscale*(2-1/(1+delta)); yscale = yscale*(2-1/(1+delta)); }

		if(printDeltas) std::cout << "delta: " << delta << std::endl;
	}

	void onKeyPressed(fgeal::Keyboard::Key key)
	{
		globalInputHandler(key, game, *this);

		switch(key)
		{
			case fgeal::Keyboard::KEY_R:
				noRotation = !noRotation;
				angle = 0;
				rotx = imgArrow->getWidth()/2;
				roty = imgArrow->getHeight()/2;
				break;

			case fgeal::Keyboard::KEY_S:
				noScale = !noScale;
				xscale = yscale = 1;
				break;

			case fgeal::Keyboard::KEY_F:
				flip = (flip == fgeal::Image::FLIP_NONE? fgeal::Image::FLIP_HORIZONTAL : flip == fgeal::Image::FLIP_HORIZONTAL? fgeal::Image::FLIP_VERTICAL : fgeal::Image::FLIP_NONE);
				break;

			case fgeal::Keyboard::KEY_D:
				printDeltas = !printDeltas;
				break;

			case fgeal::Keyboard::KEY_SPACE:
				whiteBg = !whiteBg;
				break;

			case fgeal::Keyboard::KEY_LEFT_CONTROL:
			case fgeal::Keyboard::KEY_RIGHT_CONTROL:
				fgeal::Image::useImageTransformSmoothingHint = !fgeal::Image::useImageTransformSmoothingHint;
				break;

			default:
				break;
		}
	}
};
